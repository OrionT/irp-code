#include "inst.h"
#include "local_search.h"
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;

void loadData(string filename, int& N, int& T, Double2d& demand, Double3d& holding_cost, Double2d& w, double& truck_capacity, int& num_trucks, Double2d& position) 
{
    ifstream fin(filename);
    fin >> N;
    fin >> T;
    for (int i=0; i<N; i++) {
        demand.push_back(Double1d(T,0));
        for (int j=0; j<T; j++) {
            fin >> demand[i][j];
        }
    }

    // read unit holding cost from file
    Double1d unit_holding(N,0);
    for (int i=0; i<N; i++) {
        fin >> unit_holding[i];
    }
    initHoldingCost(holding_cost, unit_holding, demand, N, T);

    // read (x,y) coordinates of clients on the plane
    double x_coord = 0;
    double y_coord = 0;
    for (int i=0; i<N; i++) {
        fin >> x_coord;
        fin >> y_coord;
        position.push_back(vector<double>(2,0));
        position[i][0] = x_coord;
        position[i][1] = y_coord;
    }

    getDistance(w, position, N);

    fin >> truck_capacity;
    fin >> num_trucks;
}

// compute holding cost H[v][s][t] defined as the cost incurred on day t when d^v_t is delivered on day s (cumulative fashion)
void initHoldingCost(Double3d& holding_cost, const Double1d& unit_holding, const Double2d& demand, const int N, const int T) {
    for (int v=0; v<N; v++) {
        holding_cost.push_back(Double2d(T, Double1d(T,0)));
        for (int t=0; t<T; t++) 
            for (int s=0; s<=t; s++) 
                holding_cost[v][s][t] = (t-s)*demand[v][t]*unit_holding[v];
    }
}

// compute pairwise Euclidean distance
void getDistance(Double2d& w, const Double2d& position, const int& N) {
    for (int i=0; i<N; i++) {
        w.push_back(Double1d(N,0));
        for (int j=0; j<N; j++)
            w[i][j] = sqrt(pow(position[i][0]-position[j][0],2) + pow(position[i][1]-position[j][1],2));
    }
}

double getTotalHolding(const Inst& inst, const Solution& sol)
{
    // For a node v, on the day when v is not visited, its holding cost is H[v][latest_visit][t]
    // on the day when v is visited, its holding cost is 0. Note we distinguish between those two cases
    // because latest_visit on a visit day is not that day
    double cost = 0;
    for (int v=0; v<inst.N; v++) 
        for (int t=0; t<inst.T; t++) 
           // if v is not visited on day t
           if (sol.visit_set[t].find(v) == sol.visit_set[t].end()) 
               cost += inst.H[v][sol.latest_visit[v][t]][t];
    return cost;
}

double getTotalCost(const Solution& sol) 
{
    double total_routing=0;
    for(size_t i=0; i<sol.routing_per_day.size(); i++)
        total_routing += sol.routing_per_day[i];
    return total_routing + sol.total_holding;
}

void checkSolCorrectness(const Inst& inst, const Solution& sol)
{
    DEBUG_MSG("Start outputting information about solution..." << endl);
    for (int t=0; t<inst.T; t++) {
        DEBUG_MSG("Day " << t << ": " << endl);
        Visit visit = sol.visit_set[t];
        for (auto v=visit.begin(); v!=visit.end(); v++)
            DEBUG_MSG("Node " << (*v) << ": latest_visit =  " << sol.latest_visit[*v][t] << ", t_hat = " << sol.t_hat[*v][t] << endl);
    }

    // checks whether holding cost is consistent with the deliver plan
    cout << "getTotalHolding: " << getTotalHolding(inst, sol) << ", sol.total_holding: " << sol.total_holding << endl;
    //assert(abs(getTotalHolding(inst, sol) - sol.total_holding) <= 1e-6);
}

void getDemandToDelivery(const Inst& inst, const Solution& sol, Double2d& demand_to_deliver)
{
    for (int t=0; t<inst.T; t++)
        for (int v=0; v<inst.N; v++) {
            demand_to_deliver[t][v] = 0;
            if (sol.visit_set[t].find(v) != sol.visit_set[t].end()) {
                for (int day=t; day<=sol.t_hat[v][t]; day++)
                    demand_to_deliver[t][v] += inst.demand[v][day];
            }
        }

}

vector<double> concordeTourHeuristic(const Inst& inst, const Solution& sol) 
{
    vector<double> tour_cost_per_day;
    for (int t=0; t<inst.T; t++) {    
        vector<int> tour;
        // get visit sequence via concorde
        // NOTE: tour returned here does not have depot 0 as the end visit
        double cost = getTourFromConcorde(tour, inst, sol, t);
        // get the set of subtours
         
        // run Concorde on the set of nodes in each subtour 
        double cost_subcycle = cost;
        cost_subcycle += getCostForBreakingUpTour(tour, inst, sol, t);
        double cost_reopt = getCostForBreakupTourAndReoptimize(tour, inst, sol, t);
        cout << "Naive subcycle: " << cost_subcycle << ", reoptimize: " << cost_reopt << endl;
        //assert(cost_reopt <= cost_subcycle+1e-6);
        tour_cost_per_day.push_back(min(cost_subcycle,cost_reopt));
        //tour_cost_per_day.push_back(cost);

        // break up visit sequence when accumulated demand exceeds total capacity
        //tour_cost_per_day[t] += getCostForBreakingUpTour(tour, inst, sol, t);
    }
    return tour_cost_per_day;
}

double getTourFromConcorde(vector<int>& tour, const Inst& inst, const Solution& sol, const int day)
{
    const Visit& visit = sol.visit_set[day];
    // write data file to *current* dir for concorde to process
    // in the future, better to use concorde library directly
    if (visit.size() <= 2) {
        for (auto const& v:visit)
            // did not include depot (TBD!)
            tour.push_back(v);
        if (tour.size() <= 1)
            return 0; 
        return inst.w[tour[0]][tour[1]] + inst.w[tour[1]][tour[0]];
    }
        
    // write input to "./tourInst.tsp"
    vector<int> mapping;
    writeDataForConcorde(inst.position, visit, mapping);

    system("./concorde tourInst.tsp > tourLog.txt");
    double tour_cost = readSolFromConcorde(tour, inst, visit, mapping);
    return tour_cost;
}

void writeDataForConcorde(const vector<vector<double> >& position, const Visit& visit, vector<int>& map_from_concorde_label_to_true_label)
{
	// Remove any existing input and output files
	std::remove("./tourInst.tsp");
	std::remove("./tourLog.txt");

    // Open a temprary input file
    char filename[1000];
    sprintf(filename, "./tourInst.tsp");
    std::ofstream fout;
    fout.open(filename, std::ofstream::out | std::ofstream::app);

    fout << "NAME : tourInst" << endl;
    fout << "COMMENT : tour of a given subset of vertices" << endl;
    fout << "TYPE : TSP" << endl;
    fout << "DIMENSION : " << visit.size() << endl;
    fout << "EDGE_WEIGHT_TYPE : EUC_2D" << endl;
    fout << "NODE_COORD_SECTION" << endl;
    for (auto const& v:visit) {
        map_from_concorde_label_to_true_label.push_back(v);
        fout << v << " " << position[v][0] << " " << position[v][1] << endl;
    } 
    fout << "EOF" << endl;

    //finalize input
    fout.close();
}

double readSolFromConcorde(vector<int>& tour, const Inst& inst, const Visit& visit, const vector<int>& mapping)
{
    vector<int> map_from_tsplabel_to_old_label;
    for (auto const& v:visit) 
        map_from_tsplabel_to_old_label.push_back(v);
    
    // read output of concorde from file "./tourInst.sol"
    // open the solution file singleTourInstPD.sol
    char solFilename[1000];
    // directory to read input
    sprintf(solFilename, "tourInst.sol");
    std::ifstream fin(solFilename);

    // the first value in singleTourInstPD.sol is the number of visited vertices
    size_t tspLength;
    fin >> tspLength;
    assert(tspLength == visit.size());

    // starting node from sol file might not be the depot (but why! concorde!)
    vector<int> cycle = init1D<int>(tspLength);

    for (size_t i = 0; i < tspLength; i++)
        fin >> cycle[i];
    
    vector<int> tour_tmp;
    for (size_t i=0; i<tspLength; i++)
        tour_tmp.push_back(mapping[cycle[i]]);

    vector<int> tour_copy = getCycleStartingFromDepot(tour_tmp);
    assert(tour_copy[0] == 0);
    for (auto const& v:tour_copy)
        tour.push_back(v);

    double tour_cost = 0;
    //double sanity_tour_cost = 0;
    assert(tour.size() >= 2);
    for (size_t i=0; i<tspLength-1; i++) {
        tour_cost += inst.w[mapping[cycle[i]]][mapping[cycle[i+1]]];
        //sanity_tour_cost += inst.w[tour[i]][tour[i+1]];
    }
    tour_cost += inst.w[mapping[cycle[tspLength-1]]][mapping[cycle[0]]];
    //sanity_tour_cost += inst.w[tour[tspLength-1]][tour[0]];
    //assert(tour_cost == sanity_tour_cost);
    //cout << "TRUE COST: " << tour_cost << "!!!!!!!!" << endl;
    return tour_cost;
}
    
vector<int> getCycleStartingFromDepot(const vector<int>& cycle_old, const int depot_index)
{
    // create a cycle vector which starts from the depot
    // first find the index of the depot
    int index_of_depot_in_this_cycle = 0;
    for (auto const& v:cycle_old) {
        if (v==depot_index) 
            break;
        index_of_depot_in_this_cycle++;
    }

    vector<int> cycle(cycle_old.size(), -1);
    for (size_t i=0; i<cycle.size(); i++)
        cycle[i] = cycle_old[(i+index_of_depot_in_this_cycle)%cycle.size()];
    return cycle;
}

double getCostForBreakupTourAndReoptimize(const vector<int>& tour, const Inst& inst, const Solution& sol, const int day)
{
    if (tour.size()<=1) return 0.0;
    if (tour.size()==2) return 2*inst.w[tour[0]][tour[1]];
    vector<double> demand(inst.N, 0);
    getDemandToDeliver(demand, inst, sol, day);
    double accum_demand = 0;
    vector<Visit> subtour_set; // set of visits (subtours)
    Visit subtour;
    // push a new visit to the container once capacity full
    int last_day_to_add = -1;
    cout << "Tour size: " << tour.size() << endl;
    cout << "Tour: " << tour[0];
    for(size_t i=1; i<tour.size(); i++)
        cout << "->" <<tour[i];
    cout << endl;
    for (size_t i=0; i<tour.size(); i++) {
        if (accum_demand-inst.truck_capacity>-1e-6) {
            assert(i!=0);
            subtour_set.push_back(subtour);
            subtour.clear();
            subtour.insert(0);
            if (abs(accum_demand-inst.truck_capacity)>1e-6)
                // need to come back to pick up remaining demand
                subtour.insert(tour[i-1]);
            last_day_to_add = i;
            cout << last_day_to_add << "th visit, accum_demand: " << accum_demand << endl;
            accum_demand -= inst.truck_capacity;
        }
        subtour.insert(tour[i]);
        //cout << "Inserted " << tour[i] << ", size: " << subtour.size() << endl;
        accum_demand += demand[tour[i]];
    }
    if (last_day_to_add != inst.T-1)
        subtour_set.push_back(subtour);
    assert((int)subtour_set.size() <= inst.num_trucks);
    double subtour_cost = 0;
    vector<vector<int> > ordered_subtours;
    for (auto const& v:subtour_set) {
        if (v.size()<=1)
            continue;
        if (v.size()==2) {
            vector<int> tmp;
            for (auto const& i:v) tmp.push_back(i);
            subtour_cost += 2*inst.w[tmp[0]][tmp[1]];
            ordered_subtours.push_back(tmp);
            continue;
        }
        vector<int> mapping;
        writeDataForConcorde(inst.position, v, mapping);
        system("./concorde tourInst.tsp > tourLog.txt");
        vector<int> subt;
        subtour_cost += readSolFromConcorde(subt, inst, v, mapping);
        ordered_subtours.push_back(subt);
    } 
    return subtour_cost;
}

double getCostForBreakingUpTour(const vector<int>& tour, const Inst& inst, const Solution& sol, const int day) 
{
    double cost_for_breaking_up_tour = 0;
    vector<double> demand(inst.N, 0);
    getDemandToDeliver(demand, inst, sol, day);
    double accum_demand = 0;
    for (size_t v=0; v<tour.size(); v++) {
        if (accum_demand > inst.truck_capacity) {
            assert(v!=0);
            int prev = tour[v-1];
            int curr = tour[v];
            if (accum_demand == inst.truck_capacity) 
                // go v-1 -> 0, 0 -> v, won't use edge (v-1,v)
                cost_for_breaking_up_tour += inst.w[prev][0]+inst.w[0][curr]-inst.w[prev][curr];
            else

                // go v-1 -> 0, 0->v-1 (here the assumption is that demand is splittable
                cost_for_breaking_up_tour += inst.w[prev][0] + inst.w[0][prev];
            accum_demand -= inst.truck_capacity;
        }
        accum_demand += demand[tour[v]];
    }
    return cost_for_breaking_up_tour;
}

void replaceExt(string& s, const string& newExt) {
   string::size_type i = s.rfind('.', s.length());

   if (i != string::npos) {
      s.replace(i+1, newExt.length(), newExt);
   }
}
void writeSolZeroHolding(const Inst& inst, const Solution& sol, const vector<double>& tour_cost_per_day, string filename)
{
    // filename = "../data/instance_folder/instance_name", replace data with result
    string sol_file = filename;
    string tmp_file = filename;
    auto pos = filename.find("data");
    tmp_file.replace(pos, 4, "tmp2"); // store temporary info used by or-tool
    sol_file.replace(pos, 4, "result"); // store concorde heuristic sol
    Double2d demand_to_deliver = init2D<double>(inst.T, inst.N);
    getDemandToDelivery(inst, sol, demand_to_deliver);
    ofstream fout(tmp_file);
    // print out total holding on the first line
    fout << sol.total_holding << endl;
    for (int t=0; t<inst.T; t++) {
       for (int v=0; v<inst.N; v++)
          fout << demand_to_deliver[t][v] << " "; 
       fout<<endl;
    }
    fout.close();

    // output solution file to ../result/*.concorde
    string output_file = sol_file;
    replaceExt(output_file, "concorde2");
    ofstream fout2(output_file);
    for (int t=0; t<inst.T; t++)
        fout2 << tour_cost_per_day[t] << endl;
    fout2.close();

    /*
    string tree_cost_file = sol_file;
    replaceExt(tree_cost_file, "tree");
    ofstream f_tree(tree_cost_file);
    for (int t=0; t<inst.T; t++)
        f_tree << sol.routing_per_day[t] << endl;
    f_tree.close();
    */
}

void writeSol(const Inst& inst, const Solution& sol, const vector<double>& tour_cost_per_day, string filename)
{
    // filename = "../data/instance_folder/instance_name", replace data with result
    string sol_file = filename;
    string tmp_file = filename;
    auto pos = filename.find("data");
    tmp_file.replace(pos, 4, "tmp"); // store temporary info used by or-tool
    sol_file.replace(pos, 4, "result"); // store concorde heuristic sol
    Double2d demand_to_deliver = init2D<double>(inst.T, inst.N);
    getDemandToDelivery(inst, sol, demand_to_deliver);
    ofstream fout(tmp_file);
    // print out total holding on the first line
    fout << sol.total_holding << endl;
    for (int t=0; t<inst.T; t++) {
       for (int v=0; v<inst.N; v++)
          fout << demand_to_deliver[t][v] << " "; 
       fout<<endl;
    }
    fout.close();

    // output solution file to ../result/*.concorde
    string output_file = sol_file;
    replaceExt(output_file, "concorde");
    ofstream fout2(output_file);
    for (int t=0; t<inst.T; t++)
        fout2 << tour_cost_per_day[t] << endl;
    fout2.close();

    string tree_cost_file = sol_file;
    replaceExt(tree_cost_file, "tree");
    ofstream f_tree(tree_cost_file);
    for (int t=0; t<inst.T; t++)
        f_tree << sol.routing_per_day[t] << endl;
    f_tree.close();
}
        
/*
TourSol getTourFromORTool(const Solution& sol, const Inst& inst) 
{
    TourSol tour_sol;
    for (size_t i=0; i<sol.visit_set.size(); i++)
        tour_sol.push_back(getTourOnDay(sol, inst, i));
    return tour_sol;
}

TourSet getTourOnDay(const Solution& sol, const Inst& inst, const int day)
{
    // use solver to compute the optimal tour given a set of nodes to be visited and demands among them
    TourSet tour_set;
    // first compute cumulative demand
    const Visit& visit = sol.visit_set[day];
    Double1d demand_cumul; // cumulative demand from day to t_hat of each guy
    for (int v=0; v<inst.N; v++) {
        double demand = 0;
        for (int i=day; i<=sol.t_hat[v][i]; i++)
            demand += inst.demand[v][i];
        demand_cumul.push_back(demand);
    }
    // then call or-tool solver
    const RoutingIndexManager::NodeIndex kDepot(0);
    RoutingIndexManager manager(inst.N, (int64)inst.truck_capacity, kDepot);
    RoutingModel routing(manager);
    return tour_set;
}
*/
