#include "inst.h"
#include <cmath>
#include <vector>
#include <assert.h>
#include <fstream>
#include <iostream>

using namespace std;

#define OUTPUT

void loadData(const char* filename, Double2d& d, Double1d& h, Double2d& c, double& Q) 
    // d: demand,
    // h: unit holding cost
    // c: edge cost
    // Q: truck capacity
{
    int N, T;
    ifstream fin(filename);
    fin >> N;
    fin >> T;

#ifdef OUTPUT
    std::cout << "Finished reading N, T" << std::endl;
#endif

    for (int i=0; i<N; i++) {
        d.push_back(Double1d(T,0));
        for (int j=0; j<T; j++) {
            fin >> d[i][j];
        }
    }

#ifdef OUTPUT
    std::cout << "Finished reading d" << std::endl;
#endif

    // read unit holding cost from file
    h.resize(N);
    for (int i=0; i<N; i++) {
        fin >> h[i];
    }

#ifdef OUTPUT
    std::cout << "Finished reading h" << std::endl;
#endif

    //initHoldingCost(h, unit_holding, d, N, T);

    // read (x,y) coordinates of clients on the plane
    double x_coord = 0;
    double y_coord = 0;
    Double2d position;
    for (int i=0; i<N; i++) {
        fin >> x_coord;
        fin >> y_coord;
        position.push_back(vector<double>(2,0));
        position[i][0] = x_coord;
        position[i][1] = y_coord;
    }

    getDistance(c, position, N);

    fin >> Q;
}

// compute pairwise Euclidean distance
void getDistance(Double2d& w, const Double2d& position, const int& N) {
    for (int i=0; i<N; i++) {
        w.push_back(Double1d(N,0));
        for (int j=0; j<N; j++)
            w[i][j] = sqrt(pow(position[i][0]-position[j][0],2) + pow(position[i][1]-position[j][1],2));
    }
}
