// Use two local search heuristics to solve inventory routing problem with truck capacity

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>
#include "inst.h"
#include "local_search.h"

#define FILE_RESULT "./heur.txt"

using namespace std;

void pcstHeuristic(const int N_prime, const int T_prime, const int K_prime, const int H_prime, const int num_inst)
{
    char filename[1000];
    //directory to read input
    sprintf(filename, "../data/n%dt%dk%dh%dinst%d.txt", N_prime, T_prime, K_prime, H_prime, num_inst); 

    const int kMaxIter = 100; // number of iterations
    const double kTer = 0.01;

// Step 1: read in data from the file
// N: # clients
// T: # days
// demand: N by T arrary for client's demand per day
// H: N*T*T arrary where H[v][s][t] is the holding cost incurred on day t when
//      the last delivery to v is on day s 
// w: N by N edge weight matrix
// truck_capacity
    int N;
    int T;
    Double2d demand;
    Double3d H;
    Double2d w;
    double truck_capacity;
    int num_trucks;
    Double2d position;
    loadData(filename, N, T, demand, H, w, truck_capacity, num_trucks, position);
    // generate big-M: large_penalty
    double large_penalty = 0;
    for (int i=0; i<N; i++)
        for (int j=0; j<i; j++)
            large_penalty += w[i][j];
    Inst inst = {
        N,
        T,
        demand,
        H,
        w,
        truck_capacity,
        num_trucks,
        large_penalty,
        position // 2d position of nodes, used in call to concorde TODO: remove this from future implementation
    };
    cout << "# clients: " << N << endl;
    // Ziye: remember to multiply unit holding cost by some constant!
    
// Step 2: init a feasible solution (periodic solution)
// For now, we will just build MST periodically
// The set of trees are stored in a vector nodes
// and edges (maybe use BGL?)
// Below is the init function to complete:
    Solution sol;
    sol.visit_set = vector<Visit>(T); // visit[d][v] = true if v is visited on day d
    //cout << "visit_size: " << sol.visit_set.size() << endl;
    sol.total_holding = 0;
    sol.routing_per_day = vector<double>(T,0);
    sol.latest_visit = init2D<int>(N,T); // latest_visit[v][s]: last visit to v before day s
    sol.t_hat = init2D<int>(N,T); // t_hat[v][s] last day after day s before visiting v again
    sol.slack_capacity = vector<double>(T,0);
    
    initPeriodicMST(sol, inst);
    Solution sol_zero_holding = initSolWithZeroHolding(sol, inst);
    /*
    double total_routing = 0;
    for (size_t t=0; t<=sol.routing_per_day.size(); t++)
        total_routing += sol.routing_per_day[t];
    cout << "Initial holding cost: " << sol.total_holding << ", routing cost: " << total_routing << endl;
    */


// Step 3: perform local search until local minima
// implement three types of local search: edit (both add and delete based on penalty), add and delete
    double time1, time2;
    localSearch(inst, sol, kTer, kMaxIter, time1);
    localSearch(inst, sol_zero_holding, kTer, kMaxIter, time2);

// Step 4: original goal is to convert Visit_set into CVRP tools using some CVRP solvers
// currently the implementation is to output visit set on each day together with their 
// demands to a txt file, and call a python script which then calls or-tools to solve
// Need a better implementation in the future   
// alternatively we can call concorde and break up tours at capacity-breaking point

    vector<double> tour_cost_per_day = concordeTourHeuristic(inst, sol);
    double total_routing_cost = 0;
    double total_tree_cost = 0;
    cout << "Concorde Heuristic..." << endl;
    cout << "Total holding: " << sol.total_holding << endl;
    for (int t=0; t<inst.T; t++) {
        double gap = 0;
        if (abs(tour_cost_per_day[t]) < 1e-6 && abs(sol.routing_per_day[t]) < 1e-6)
            gap = 0;
        else
            gap = (tour_cost_per_day[t]-sol.routing_per_day[t])/tour_cost_per_day[t]; 
        cout << "Day " << t << ", tour: " << tour_cost_per_day[t] 
            << ", tree: " << sol.routing_per_day[t] << ". Gap: " << gap << endl; 
        //assert(gap > -1e-6);
        total_routing_cost += tour_cost_per_day[t];
        total_tree_cost += sol.routing_per_day[t];
    }
    double total = total_routing_cost+sol.total_holding;

    // for sol2 (zero holding initial sol)
    vector<double> tour_cost2 = concordeTourHeuristic(inst, sol_zero_holding);
    double total_routing_cost2 = 0;
    double total_tree_cost2 = 0;
    for (int t=0; t<inst.T; t++) {
        total_routing_cost2 += tour_cost2[t];
        total_tree_cost2 += sol_zero_holding.routing_per_day[t];
    }
    double total2 = total_routing_cost2+sol_zero_holding.total_holding;
    
    cout << "Total tree routing: " << total_tree_cost << 
        ", " << total_tree_cost2 << endl;
    cout << "Total routing from concorde heuristics: " << total_routing_cost << ", " << total_routing_cost2 << endl;
    cout << "Total cost using concorde heuristic: " << total << ", " << total2 << endl;
    //cout << "Writing solution file to ../result/instance_folder/ " << endl;
    //writeSol(inst, sol, tour_cost_per_day, filename);
    //writeSolZeroHolding(inst, sol_zero_holding, tour_cost2, filename);

    double best_routing=total_routing_cost, 
           best_holding=sol.total_holding, best_total=total,
           best_time = time1;
    if (total2 < total) {
        best_routing = total_routing_cost2;
        best_holding = sol_zero_holding.total_holding;
        best_total = total2;
        best_time = time2;
    }

    // write sol to file
    char outputname[1000];
    sprintf(outputname, FILE_RESULT);
    std::ofstream fout;
    fout.open(outputname, std::ofstream::out | std::ofstream::app);
	fout << N_prime << ' ' << T_prime << ' ' << K_prime << ' ' << H_prime << ' ' << num_inst << ' ' 
        << best_routing << ' ' << best_holding << ' ' << best_total << ' ' << best_time << endl;
    fout.close();
}

/*
int main()
{
    // run on bugs
    pcstHeuristic(30, 11, 5, 10, 4);
    return 0;
}
*/

int main() 
{
    char outputname[1000];
    sprintf(outputname, FILE_RESULT);
    std::ofstream fout;
    fout.open(outputname, std::ofstream::out | std::ofstream::app);
    fout << "N T K H inst routing holding alg_total alg_time" << endl;
    int H(30);
    for (int N=40; N<=70; N+=10) 
        for (int T=3; T<=9; T+=2) 
            for (int K=1; K<=4; K++) 
                for (int inst=0; inst<10; inst++)
                    pcstHeuristic(N, T, K, H, inst);
    
   fout.close();
   return 0; 
}
