/* standard library includes */
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>

/* scip includes */
#include "objscip/objscip.h"
#include "objscip/objscipdefplugins.h"

/* user defined includes */
#include "inst_mip.h"
#include "ConshdlrGSEC.h"

/* namespace usage */
using namespace std;
using namespace scip;
using namespace tsp;

#define SCIP_DEBUG
#define OUTPUT
//#define MODEL_DEBUG
//#define CONSHDLR_DEBUG
//#define SBC3 // symmetry breaking constraint 3 Adulyasak
//
#define FILE_RESULT "./mip.txt"

//------------------------------------------------------------
static double RC_EPS = 1e-4;

SCIP_RETCODE 
execmain( 
        const int N,
        const int T,
        const int K,
        const int H, // holding cost multiplier
        const int inst
        )
{
    SCIP* scip = NULL;

   /**********************
    * Setup problem data *
    **********************/
    //write to file
    char filename[255];
    sprintf(filename, "../data/n%dt%dk%dh%dinst%d.txt",
            N, T, K, H, inst);
    Double2d d; // d[i][t] customer i's demand on day t
    Double2d c; // c[i][j] edge (i,j) cost
    double Q; // truck capacity
    Double1d h; // unit holding cost for customer i
    loadData(filename, d, h, c, Q);
    

#ifdef OUTPUT
    char data_info[255];
    sprintf(data_info, "N=%d, T=%d, K=%d, H=%d, Q=%f.",
                    N, T, K, H, Q);
    std::cout << data_info << std::endl;
#endif
   /**************
    * Setup SCIP *
    **************/

   /* initialize SCIP environment */
   SCIP_CALL( SCIPcreate(&scip) );

   /***********************
    * Version information *
    ***********************/

   SCIPprintVersion(scip, NULL);
   SCIPinfoMessage(scip, NULL, "\n");

   /* include default plugins */
   SCIP_CALL( SCIPincludeDefaultPlugins(scip) );

   /* set verbosity parameter */
   SCIP_CALL( SCIPsetIntParam(scip, "display/verblevel", 5) );
   /* SCIP_CALL( SCIPsetBoolParam(scip, "display/lpinfo", TRUE) ); */
   SCIP_CALL( SCIPsetRealParam(scip, "limits/time", 3600) );

   /* create empty problem */
   SCIP_CALL( SCIPcreateProb(scip, "VRP", 0, 0, 0, 0, 0, 0, 0) );

   // add I[i][t] variables for the inventory level at the end of period t
   char var_name[255];
   vector< vector<SCIP_VAR*> > I(N);
   for (int i = 0; i < N; ++i)
   {
       I[i].resize(T, (SCIP_VAR*) NULL); /*lint !e732 !e747*/
      for (int t = 0; t < T; ++t)
      {
         SCIP_VAR* var;
         (void) SCIPsnprintf(var_name, 255, "I(%d, %d)", i, t );

         SCIP_CALL( SCIPcreateVar(scip,
                     &var,                   // returns new index
                     var_name,               // name
                     0.0,                    // lower bound
                     (t==0)?0.0:SCIPinfinity(scip),                    // upper bound
                     h[i],              // objective
                     SCIP_VARTYPE_CONTINUOUS,    // variable type
                     true,                   // initial
                     false,                  // forget the rest ...
                     NULL, NULL, NULL, NULL, NULL) );  /*lint !e732 !e747*/
         SCIP_CALL( SCIPaddVar(scip, var) );
         I[i][t] = var; /*lint !e732 !e747*/
      }
   }

  // z[k][t][i] var whether customer i is visited by vehicle k on day t
  // q[k][t][i] quantity delivered by vehicle k on day t
  vector< vector< vector<SCIP_VAR*> > > z(K);
  vector< vector< vector<SCIP_VAR*> > > q(K);
  for (int k=0; k<K; k++) 
  {
      z[k].resize(T);
      q[k].resize(T);
      for (int t=0; t<T; t++) 
      {
          z[k][t].resize(N, (SCIP_VAR*) NULL);
          q[k][t].resize(N, (SCIP_VAR*) NULL);
          for (int i=0; i<N; i++)
          {
              SCIP_VAR* var;
              (void) SCIPsnprintf(var_name, 255, "z(%d, %d, %d)", k, t, i);
              SCIP_CALL( SCIPcreateVar(scip,
                         &var,                   // returns new index
                         var_name,               // name
                         0.0,                    // lower bound
                         1.0,                    // upper bound
                         0.0,             // objective
                         SCIP_VARTYPE_BINARY,   // variable type
                         true,                   // initial
                         false,                  // forget the rest ...
                         NULL, NULL, NULL, NULL, NULL) );  /*lint !e732 !e747*/
             SCIP_CALL( SCIPaddVar(scip, var) );
             z[k][t][i] = var; /*lint !e732 !e747*/
            
             SCIP_VAR* qvar;
              (void) SCIPsnprintf(var_name, 255, "q(%d, %d, %d)", k, t, i);
              SCIP_CALL( SCIPcreateVar(scip,
                         &qvar,                   // returns new index
                         var_name,               // name
                         0.0,                    // lower bound
                         SCIPinfinity(scip),                    // upper bound
                         0.0,             // objective
                         SCIP_VARTYPE_CONTINUOUS,   // variable type
                         true,                   // initial
                         false,                  // forget the rest ...
                         NULL, NULL, NULL, NULL, NULL) );  /*lint !e732 !e747*/
             SCIP_CALL( SCIPaddVar(scip, qvar) );
             q[k][t][i] = qvar; /*lint !e732 !e747*/
          }
      }
   }
  

  vector< vector< vector< vector< SCIP_VAR* > > > > x(K);
  for (int k=0; k<K; k++)
  {
      x[k].resize(T);
      for (int t=0; t<T; t++)
      {
          x[k][t].resize(N);
          for (int i=0; i<N; i++)
          {
              x[k][t][i].resize(i, (SCIP_VAR*) NULL);
              for (int j=0; j<i; j++)
              {
                  SCIP_VAR* var;
                  (void) SCIPsnprintf(var_name, 255, "x(%d, %d, %d, %d)", k, t, i, j);
                  SCIP_CALL( SCIPcreateVar(scip,
                             &var,                   // returns new index
                             var_name,               // name
                             0.0,                    // lower bound
                             1.0,                    // upper bound
                             c[i][j],             // objective
                             SCIP_VARTYPE_BINARY,   // variable type
                             true,                   // initial
                             false,                  // forget the rest ...
                             NULL, NULL, NULL, NULL, NULL) );  /*lint !e732 !e747*/
                 SCIP_CALL( SCIPaddVar(scip, var) );
                 x[k][t][i][j] = var; /*lint !e732 !e747*/
              }
          }
      }
  }

  char con_name[255];
    // inventory balance constraint for i\in N_c (ignore depot), t\in T
    for (int i=1; i<N; i++)
    {
        for (int t=1; t<T; t++)
        {
            SCIP_CONS* con;
            (void) SCIPsnprintf(con_name, 255, "IB(%d,%d)", i, t);
            SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                          d[i][t],                    /* lhs */
                          d[i][t],                    /* rhs */
                          true,                   /* initial */
                          true,                  /* separate */
                          true,                   /* enforce */
                          true,                   /* check */
                          true,                   /* propagate */
                          false,                  /* local */
                          false,                  /* modifiable */
                          false,                  /* dynamic */
                          false,                  /* removable */
                          false) );               /* stickingatnode */
            SCIP_CALL( SCIPaddCons(scip, con) );
            SCIP_CALL( SCIPaddCoefLinear(scip, con, I[i][t-1], 1.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, con, I[i][t], -1.0) );
            for (int k=0; k<K; k++)
            {
                SCIP_CALL( SCIPaddCoefLinear(scip, con, q[k][t][i], 1.0) );
            }
            SCIP_CALL( SCIPreleaseCons(scip, &con) );
        }
    }
              
    // capacity constraint for k\in K, t\in T
    for (int k=0; k<K; k++)
    {
        for (int t=0; t<T; t++)
        {
            SCIP_CONS* con;
            (void) SCIPsnprintf(con_name, 255, "capacity(%d,%d)", k, t);
            SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                          -SCIPinfinity(scip),                    /* lhs */
                          0.0,                    /* rhs */
                          true,                   /* initial */
                          true,                  /* separate */
                          true,                   /* enforce */
                          true,                   /* check */
                          true,                   /* propagate */
                          false,                  /* local */
                          false,                  /* modifiable */
                          false,                  /* dynamic */
                          false,                  /* removable */
                          false) );               /* stickingatnode */
            SCIP_CALL( SCIPaddCons(scip, con) );
            SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][0], -Q) );
            for (int i=1; i<N; i++)
            {
                SCIP_CALL( SCIPaddCoefLinear(scip, con, q[k][t][i], 1.0) );
            }
            SCIP_CALL( SCIPreleaseCons(scip, &con) );
        }
    }

    // constraint that each node can only be visited by at most one vehicle
    for (int i=1; i<N; i++)
    {
        for (int t=0; t<T; t++)
        {
            SCIP_CONS* con;
            (void) SCIPsnprintf(con_name, 255, "vehicle visit(%d,%d)", i, t);
            SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                          -SCIPinfinity(scip),                    /* lhs */
                          1.0,                    /* rhs */
                          true,                   /* initial */
                          true,                  /* separate */
                          true,                   /* enforce */
                          true,                   /* check */
                          true,                   /* propagate */
                          false,                  /* local */
                          false,                  /* modifiable */
                          false,                  /* dynamic */
                          false,                  /* removable */
                          false) );               /* stickingatnode */
            SCIP_CALL( SCIPaddCons(scip, con) );
            for (int k=0; k<K; k++)
            {
                SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][i], 1.0) );
            }
            SCIP_CALL( SCIPreleaseCons(scip, &con) );
        }
    }

    // strengthened ub on delivered quantity
    for (int i=1; i<N; i++)
    {
        for (int k=0; k<K; k++)
        {
            for (int t=0; t<T; t++)
            {
                SCIP_CONS* con;
                (void) SCIPsnprintf(con_name, 255, "qub(%d, %d, %d)", i, k, t);
                SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                              -SCIPinfinity(scip),                    /* lhs */
                              0.0,                    /* rhs */
                              true,                   /* initial */
                              true,                  /* separate */
                              true,                   /* enforce */
                              true,                   /* check */
                              true,                   /* propagate */
                              false,                  /* local */
                              false,                  /* modifiable */
                              false,                  /* dynamic */
                              false,                  /* removable */
                              false) );               /* stickingatnode */
                SCIP_CALL( SCIPaddCons(scip, con) );
                SCIP_CALL( SCIPaddCoefLinear(scip, con, q[k][t][i], 1.0) );
                double accum_demand(0);
                for (int l=t; l<T; l++)
                    accum_demand += d[i][l];
                SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][i], 
                            Q < accum_demand - RC_EPS ? -Q : -accum_demand) );
                SCIP_CALL( SCIPreleaseCons(scip, &con) );
            }
        }
    }

    // add degree constraint
    for (int i=0; i<N; i++)
    {
        for (int k=0; k<K; k++)
        {
            for (int t=0; t<T; t++)
            {
                SCIP_CONS* con;
                (void) SCIPsnprintf(con_name, 255, "deg(%d, %d, %d)", i, k, t);
                SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                              0.0,                    /* lhs */
                              0.0,                    /* rhs */
                              true,                   /* initial */
                              true,                  /* separate */
                              true,                   /* enforce */
                              true,                   /* check */
                              true,                   /* propagate */
                              false,                  /* local */
                              false,                  /* modifiable */
                              false,                  /* dynamic */
                              false,                  /* removable */
                              false) );               /* stickingatnode */
                SCIP_CALL( SCIPaddCons(scip, con) );
                SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][i], -2.0) );
                for (int j=0; j<i; j++)
                {
                    SCIP_CALL( SCIPaddCoefLinear(scip, con, x[k][t][i][j], 1.0) );
                }
                for (int j=i+1; j<N; j++)
                {
                    SCIP_CALL( SCIPaddCoefLinear(scip, con, x[k][t][j][i], 1.0) );
                }
                SCIP_CALL( SCIPreleaseCons(scip, &con) );
            }
        }
    }

    // additional logical constraints
    for (int i=1; i<N; i++)
    {
        for (int k=0; k<K; k++)
        {
            for (int t=0; t<T; t++)
            {
                SCIP_CONS* con;
                (void) SCIPsnprintf(con_name, 255, "z_logical(%d, %d, %d)", i, k, t);
                SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                              -SCIPinfinity(scip),                    /* lhs */
                              0.0,                    /* rhs */
                              true,                   /* initial */
                              true,                  /* separate */
                              true,                   /* enforce */
                              true,                   /* check */
                              true,                   /* propagate */
                              false,                  /* local */
                              false,                  /* modifiable */
                              false,                  /* dynamic */
                              false,                  /* removable */
                              false) );               /* stickingatnode */
                SCIP_CALL( SCIPaddCons(scip, con) );
                SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][i], 1.0) );
                SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][0], -1.0) );
                SCIP_CALL( SCIPreleaseCons(scip, &con) );
            }
        }
    }

    // additional logical constraints
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<i; j++)
        {
            for (int k=0; k<K; k++)
            {
                for (int t=0; t<T; t++)
                {
                    SCIP_CONS* con_i;
                    (void) SCIPsnprintf(con_name, 255, "x_logical_%d(%d, %d, %d, %d)", i, i, j, k, t);
                    SCIP_CALL( SCIPcreateConsLinear(scip, &con_i, con_name, 0, 0, 0,
                                  -SCIPinfinity(scip),                    /* lhs */
                                  0.0,                    /* rhs */
                                  true,                   /* initial */
                                  true,                  /* separate */
                                  true,                   /* enforce */
                                  true,                   /* check */
                                  true,                   /* propagate */
                                  false,                  /* local */
                                  false,                  /* modifiable */
                                  false,                  /* dynamic */
                                  false,                  /* removable */
                                  false) );               /* stickingatnode */
                    SCIP_CALL( SCIPaddCons(scip, con_i) );
                    SCIP_CALL( SCIPaddCoefLinear(scip, con_i, x[k][t][i][j], 1.0) );
                    SCIP_CALL( SCIPaddCoefLinear(scip, con_i, z[k][t][i], -1.0) );
                    SCIP_CALL( SCIPreleaseCons(scip, &con_i) );

                    SCIP_CONS* con_j;
                    (void) SCIPsnprintf(con_name, 255, "x_logical_%d(%d, %d, %d, %d)", j, i, j, k, t);
                    SCIP_CALL( SCIPcreateConsLinear(scip, &con_j, con_name, 0, 0, 0,
                                  -SCIPinfinity(scip),                    /* lhs */
                                  0.0,                    /* rhs */
                                  true,                   /* initial */
                                  true,                  /* separate */
                                  true,                   /* enforce */
                                  true,                   /* check */
                                  true,                   /* propagate */
                                  false,                  /* local */
                                  false,                  /* modifiable */
                                  false,                  /* dynamic */
                                  false,                  /* removable */
                                  false) );               /* stickingatnode */
                    SCIP_CALL( SCIPaddCons(scip, con_j) );
                    SCIP_CALL( SCIPaddCoefLinear(scip, con_j, x[k][t][i][j], 1.0) );
                    SCIP_CALL( SCIPaddCoefLinear(scip, con_j, z[k][t][i], -1.0) );
                    SCIP_CALL( SCIPreleaseCons(scip, &con_j) );
                }
            }
        }
    }

    // strengthened inequality to infer lb on I[i][t-s] for i\in N_c, t\in T, s\in [t-1]
    for (int i=1; i<N; i++) 
    {
        for (int t=0; t<T; t++)
        {
            for (int s=0; s<t; s++)
            {
                double remain_demand(0);
                for (int l=0; l<=s; l++)
                    remain_demand += d[i][l];
                SCIP_CONS* con;
                (void) SCIPsnprintf(con_name, 255, "I_(%d, %d, %d)", i, t, s);
                SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                              remain_demand,                    /* lhs */
                              SCIPinfinity(scip),                    /* rhs */
                              true,                   /* initial */
                              true,                  /* separate */
                              true,                   /* enforce */
                              true,                   /* check */
                              true,                   /* propagate */
                              false,                  /* local */
                              false,                  /* modifiable */
                              false,                  /* dynamic */
                              false,                  /* removable */
                              false) );               /* stickingatnode */
                SCIP_CALL( SCIPaddCons(scip, con) );
                SCIP_CALL( SCIPaddCoefLinear(scip, con, I[i][t-s-1], 1.0) );
                for (int k=0; k<K; k++) 
                {
                    for (int l=0; l<=s; l++)
                    {
                        SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t-l][i], remain_demand) );
                    }
                }
                SCIP_CALL( SCIPreleaseCons(scip, &con) );
            }
        }
    }

    // symmetry breaking for dispatching vehicles: z[0][k-1][t] >= z[0][k][t] (SBC0 in Adulysak)
    for (int k=1; k<K; k++)
    {
        for (int t=0; t<T; t++)
        {
            SCIP_CONS* con;
            (void) SCIPsnprintf(con_name, 255, "SB0_(%d, %d)", k, t);
            SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                          0.0,                    /* lhs */
                          SCIPinfinity(scip),                    /* rhs */
                          true,                   /* initial */
                          true,                  /* separate */
                          true,                   /* enforce */
                          true,                   /* check */
                          true,                   /* propagate */
                          false,                  /* local */
                          false,                  /* modifiable */
                          false,                  /* dynamic */
                          false,                  /* removable */
                          false) );               /* stickingatnode */
            SCIP_CALL( SCIPaddCons(scip, con) );
            SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k-1][t][0], 1.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][0], -1.0) );
            SCIP_CALL( SCIPreleaseCons(scip, &con) );
        }
    }
    
#ifdef SBC3 // currently results in large number numerical issues
    // symmetry breaking for visiting customers (lexico order) (SBC3)
    for (int j=1; j<N; j++)
    {
        for (int k=1; k<K; k++)
        {
            for (int t=0; t<T; t++)
            {
                SCIP_CONS* con;
                (void) SCIPsnprintf(con_name, 255, "SB3_(%d, %d, %d)", j, k, t);
                SCIP_CALL( SCIPcreateConsLinear(scip, &con, con_name, 0, 0, 0,
                              0.0,                    /* lhs */
                              SCIPinfinity(scip),                    /* rhs */
                              true,                   /* initial */
                              true,                  /* separate */
                              true,                   /* enforce */
                              true,                   /* check */
                              true,                   /* propagate */
                              false,                  /* local */
                              false,                  /* modifiable */
                              false,                  /* dynamic */
                              false,                  /* removable */
                              false) );               /* stickingatnode */
                SCIP_CALL( SCIPaddCons(scip, con) );
                for (int i=1; i<=j; i++)
                {
                    SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k-1][t][i], pow(2, j-i)) );
                    SCIP_CALL( SCIPaddCoefLinear(scip, con, z[k][t][i], -pow(2, j-i)) );
                }
                SCIP_CALL( SCIPreleaseCons(scip, &con) );
            }
        }
    }
#endif

   // include constraint handler for generalized subtours
   // const char* CONSHDLR_OUTPUT = "./conshdlr_info.scip";
   // ofstream conshdlr_out(CONSHDLR_OUTPUT, std::ios::out);
   // conshdlr_out.close();
   
   // GSEC constraint on each k\in K, t\in T

   SCIP_CALL( SCIPincludeObjConshdlr(scip, 
               new ConshdlrSubtour(scip), TRUE) );

   // due to some ill design, we need xtmp[k][t][i][j] and ztmp[k][t][i]
   /*
   vector< vector< vector< vector< SCIP_VAR* 
       > > > > xtmp(K);
   vector< vector< vector< SCIP_VAR* 
       > > > ztmp(K);
   for (int k=0; k<K; k++)
   {
       xtmp[k].resize(T);
       ztmp[k].resize(T);
       for (int t=0; t<T; t++) 
       {
           xtmp[k][t].resize(N);
           ztmp[k][t].resize(N, (SCIP_VAR*) NULL);
           for (int i=0; i<N; i++)
           {
               xtmp[k][t][i].resize(i, (SCIP_VAR*) NULL);
               ztmp[k][t][i] = z[i][k][t];
               for (int j=0; j<i; j++)
               {
                   xtmp[k][t][i][j] = x[i][j][k][t];
               }
           }
       }
   }
   */

   for (int k=0; k<K; k++)
   {
       for (int t=0; t<T; t++)
       {
           SCIP_CONS* cons;
           (void) SCIPsnprintf(con_name, 255, "subtour_(%d, %d)", k, t);
           SCIP_CALL( SCIPcreateConsSubtour(scip, 
                       &cons, 
                       con_name, 
                       x[k][t], z[k][t], k, t,
                       FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, TRUE)
                       );
           SCIP_CALL( SCIPaddCons(scip, cons) );
           SCIP_CALL( SCIPreleaseCons(scip, &cons) );
       }
   }

   /*************
    *  Solve    *
    *************/
#ifdef CONSHDLR_DEBUG
   /*
    for (int k=0; k<K; k++)
        for (int t=0; t<T; t++)
        {
            std::cout << "Taking address." << std::endl;
           vector< SCIP_VAR* >* tmp = &(ztmp[k][t]);
           std::cout << "(" << k <<", " << t << ")" << std::endl;
           std::cout << tmp->size() << std::endl;
        }
        */
#endif

    //auto start = std::chrono::high_resolution_clock::now();
    SCIP_CALL( SCIPsolve(scip) );
    auto ub = SCIPgetPrimalbound(scip);
    auto lb = SCIPgetDualbound(scip);
    auto time = SCIPgetSolvingTime(scip);
    std::ofstream out(FILE_RESULT, std::ios::out | std::ios::app);
    char info[255];
    sprintf(info, "%d %d %d %d %d %f %f %f", N, T, K, H, inst, ub, lb, time);
    out << info << std::endl;
    out.close();
    out.clear();
    //auto end = std::chrono::high_resolution_clock::now();
    /*
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
    duration *= 1e-9;
   ub = SCIPgetPrimalbound(scip);
   lb = SCIPgetDualbound(scip);
   */

   /**************
    * Statistics *
    *************/
    SCIP_CALL( SCIPprintStatistics(scip, NULL) );

    SCIP_CALL( SCIPprintBestSol(scip, NULL, FALSE) );

   /********************
    * Deinitialization *
    ********************/
    for (int i=0; i<N; i++)
    {
        for (int t=0; t<T; t++)
        {
            SCIP_CALL( SCIPreleaseVar( scip, &I[i][t] ) );
        }
    }

    for (int i=0; i<N; i++)
    {
        for (int k=0; k<K; k++)
        {
            for (int t=0; t<T; t++)
            {
                SCIP_CALL( SCIPreleaseVar( scip, &q[k][t][i] ) );
                SCIP_CALL( SCIPreleaseVar( scip, &z[k][t][i] ) );
            }
        }
    }
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<i; j++)
        {
            for (int k=0; k<K; k++)
            {
                for (int t=0; t<T; t++)
                {
                    SCIP_CALL( SCIPreleaseVar( scip, &x[k][t][i][j] ) );
                }
            }
        }
    }

    //SCIP_CALL( SCIPfree(&scip) );
    BMScheckEmptyMemory();
    return SCIP_OKAY;
}

int main()
{
    std::ofstream out(FILE_RESULT, std::ios::out | std::ios::app);
    out << "N T K H inst ub lb time" << std::endl;
    out.close();
    out.clear();
    for (int N=40; N<=70; N+=10)
        for (int K=1; K<=4; K++)
            for (int T=3; T<=9; T+=2)
                for (int inst=0; inst<10; inst++)
                    execmain(N, T, K, 30, inst);
}
