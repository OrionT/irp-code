#include <vector>
#include <fstream>
#include <iostream>
#include <unordered_set>
#include <map>
#include "local_search.h"
#include "inst.h"
#include "assert.h"
#include <chrono>
#include <ctime>
using namespace std;
using namespace std::chrono;

bool SANITY_CHECK = false;
const int kMaxIter = 1e3; // iter limit for lagrangian relaxation
const double kThreshLagProgress = 1e-5; // threshold to half eps in lagrangian relaxation when progress is slow

void getPCST(unordered_set<int>& visit, double& routing_cost, const Inst& inst, const vector<double>& _penalties)
{
    // This function calls dapcst solver for steiner tree instance given by the input
    // inherited those arg from Yang's old code
    // too lazy to replace all those words by inst. blabla, so just assign them values here
    const int _depot = 0;
    const int _numNodes = inst.N;
    const int _numArcs = inst.N*(inst.N-1);
    const Double2d _edgeWeights = inst.w;

    // init visit and routing_cost
    routing_cost = 0;
    visit.clear();
    // done checking

	// Remove any existing input and output files
	std::remove("testInst.stp");
	std::remove("testSoln.txt");

	// Open a temprary input file
	char filename[1000];
	sprintf(filename, "testInst.stp");
	std::ofstream fout;
	fout.open(filename, std::ofstream::out | std::ofstream::app);

	// Fill in input file
	fout << "33D32945 STP File, STP Format Version 1.0" << std::endl;
	fout << std::endl;
	fout << "SECTION Graph" << endl;
	fout << "Nodes " << _numNodes << endl;
	fout << "Arcs " << _numArcs << endl;
	for (int u = 0; u < _numNodes; u++){
		for (int v = 0; v < _numNodes; v++){
			if(v != u){
				fout << "A " << u + 1 << " " << v + 1 << " " << _edgeWeights.at(u).at(v) << endl;
                /*
				if (!_existingTree.at(u).at(v)){
					fout << "A " << u + 1 << " " << v + 1 << " " << _edgeWeights.at(u).at(v) << endl;
				}
				else{
					//those edge already in the existing tree are free to use in the PCST
					fout << "A " << u + 1 << " " << v + 1 << " " << 0 << endl;
				}
                */
			}
		}
	}

	fout << endl;
	fout << "SECTION Terminals" << endl;
	fout << "Terminals " << _numNodes << endl;
    // penalties
	for (int v = 0; v < _numNodes; v++){
		fout << "TP " << v+1 << " " << _penalties.at(v) << endl;
	}
	fout << std::endl;
	fout << "RootP " << _depot+1 << endl;
	fout << "END" << endl;
	fout << endl;
	fout << "EOF" << endl;

	// Finalize input
	fout.close();

	// Run executable and create output to "testSoln.txt"
	system("./dapcstp -f testInst.stp -o testSoln.txt 1>/dev/null 2>/dev/null");

    // extract sol info from txt output
	vector<vector<bool> > edges = init2D<bool>(_numNodes,_numNodes);
	FILE *fp;
	char buf[256];

	int e1, e2; // current edge endpoints //

	// Open the solution file
	if ((fp = fopen("testSoln.txt", "r")) == NULL) {
		fflush(stderr);
		fprintf(stderr, "error parse_output: file not found testSoln\n");
	}

	while (fgets(buf, 256, fp) != NULL) {
		if (sscanf(buf, "E %d %d", &e1, &e2) == 2) {
			// Add edge e1->e2 to adjacency matrix
			edges.at(e1-1).at(e2-1) = true;
            routing_cost += _edgeWeights[e1-1][e2-1];
            visit.insert(e1-1);
            visit.insert(e2-1);
		}
	}

    //cerr << "Routing cost: " << routing_cost << endl;

	fclose(fp);

    /*
    for (int i=0; i<_numNodes; i++)
        if (visit.find(i) == visit.end())
            DEBUG_MSG(i << " is not visited." << endl);
    */

}

Solution initSolWithZeroHolding(const Solution& sol, const Inst& inst)
{
    assert(sol.visit_set[0].size() == (size_t)inst.N);
    Solution sol2;
    sol2.visit_set = vector<Visit>(inst.T, sol.visit_set[0]);
    sol2.total_holding = 0;
    sol2.routing_per_day = vector<double>(inst.T, sol.routing_per_day[0]);
    sol2.latest_visit = init2D<int>(inst.N, inst.T);
    sol2.t_hat = init2D<int>(inst.N, inst.T);
    for (int t=0; t<inst.T; t++)
        for (int v=0; v<inst.N; v++) {
            sol2.latest_visit[v][t] = t-1;
            sol2.t_hat[v][t] = t;
        }
    sol2.slack_capacity = vector<double>(inst.T,0);
    // TODO: compute sol2 slack
    for (int t=0; t<inst.T; t++) {
        double total_demand = 0;
        for (int v=0; v<inst.N; v++)
            total_demand += inst.demand[v][t];
        sol2.slack_capacity[t] = inst.num_trucks*inst.truck_capacity-total_demand;
    }
    return sol2;
}

void initPeriodicMST(Solution& sol, const Inst& inst)
{
    // This function initializes the periodic MST spanning all nodes 
    // w.r.t. truck capacity constraint, i.e. as long as total cumulative demand
    // does not exceed total truck capacity, we will not send trucks on that day
    // This function updates solution info accordingly.
    int N = inst.N;
    int T = inst.T;
    const Double2d demand = inst.demand;
    const Double2d w = inst.w;
	//set large enough penalty for PCST to force it to visit all clients at time 0
	double largePenalty = 0;
	for (int u = 0; u < N; u++){
		for (int v = 0; v < N; v++){
			largePenalty += w.at(u).at(v);
		}
	}
	
	vector<double> largePenalties = init1D<double>(N);
	for (int v = 0; v < N; v++){
		largePenalties.at(v) = largePenalty;
	}

	//create pcst instance to find initial MST for time 0
    unordered_set<int> visit_new;
    double routing_cost = 0;
	getPCST(visit_new, routing_cost, inst, largePenalties);
    assert(visit_new.size() == (size_t)inst.N);
    cout << "MST cost: " << routing_cost << endl;
    
    // figure out when to build the tree so the truck capacity is respected
    // we first calculate aggregated demand per day and does not put MST as long as truck capacity is not violated
    vector<double> demand_per_day(T,0);
    for (int i=0; i<T; i++)
        for (int j=0; j<N; j++)
            demand_per_day[i] += inst.demand[j][i];
    vector<int> delivery_day; //indicate which day needs to visit everyone in the init sol
    delivery_day.push_back(0);
    DEBUG_MSG("Truck capacity: " << inst.truck_capacity << ". num_trucks: " << inst.num_trucks << endl);
    double total_capacity = inst.truck_capacity * inst.num_trucks;
    DEBUG_MSG("Total capacity: " << total_capacity << endl);
    double curr_total = 0;
    for (int i=0;i<T;i++) {
        // feasibility check
        //cout << "Demand on day " << i << ": " << demand_per_day[i] << endl;
        if (demand_per_day[i] > total_capacity) {
                // problem infeasible since current day's demand exceeds total capacity
                cerr << "Infeasibility detected on day " << i <<". Abort." << endl;
                exit(0);
        }
        curr_total += demand_per_day[i];
        DEBUG_MSG("Current total demand: " << curr_total << endl);
        if (curr_total > total_capacity) {
            DEBUG_MSG("Day " << i << " exceeds total capacity." << endl);
            //cout << "Slack: " << inst.num_trucks*inst.truck_capacity-curr_total+demand_per_day[i] << endl;
            delivery_day.push_back(i);
            curr_total = demand_per_day[i];
        }
    }

    for (auto const& v:delivery_day) {
        cout << "Day " << v << " needs to be visited." << endl;
        sol.visit_set[v] = visit_new;
    }

    initSol(sol, delivery_day, routing_cost, inst);

    // sanity check for correctness of initSol
    DEBUG_MSG("Sanity check for initSol starts..."<<endl);
    for (size_t i=1; i<delivery_day.size(); i++) {
            for (int v=0; v<inst.N; v++) {
                if (delivery_day[i]-1 != delivery_day[i-1])
                    assert(sol.latest_visit[v][delivery_day[i]-1] == delivery_day[i-1]);
                assert(sol.t_hat[v][delivery_day[i]] == inst.T-1 || sol.t_hat[v][delivery_day[i]] == delivery_day[i+1]-1);
            }
    }
    DEBUG_MSG("Sanity check for initSol passes."<<endl);
}

void initSol(Solution& sol, const vector<int>& delivery_day, const double routing_cost, const Inst& inst)
{
    // This function initializes solution according to periocidic MST
    // during delivery_day it is built. And update solution info accordingly
   
    // edge case: on day 0, latest_visit is not well-defined. Define it as -1
    for (int v=0; v<inst.N; v++) 
        sol.latest_visit[v][0] = -1;
    
    for (size_t i=0; i<delivery_day.size(); i++) {
        sol.routing_per_day[delivery_day[i]] = routing_cost;

        
        // if i=delivery_day.size()-1, i+1 will go out of bounds, split cases below
        if (i < delivery_day.size()-1) {
            // This part is a bit tricky, latest_visit of a day where a guy
            // gets delivered is not that day, but the last time he gets visited. This definition works when calculating saving 
            for (int j=delivery_day[i]+1; j<=delivery_day[i+1]; j++) 
                for (int v=0; v<inst.N; v++) 
                    sol.latest_visit[v][j] = delivery_day[i];
            // t_hat's range for j is different from above, so need another loop
            for (int j=delivery_day[i]; j<delivery_day[i+1]; j++)
                for (int v=0; v<inst.N; v++)
                    sol.t_hat[v][j] = delivery_day[i+1]-1;

            /*
            // compute slack matrix
            double demand_per_period = 0;
            for (int t=delivery_day[i]; t<=delivery_day[i+1]-1; t++) {
                // day t is not delivered in periodic MST
                if (t!=delivery_day[i])
                    sol.slack_capacity[t] = inst.num_trucks*inst.truck_capacity;
                demand_per_period += demand_per_day[t];
            }
            //DEBUG_MSG("Demand per period: " << demand_per_period << endl);
            sol.slack_capacity[delivery_day[i]] = inst.num_trucks*inst.truck_capacity-demand_per_period;
            */
        } else {
            // i is the last day that gets delivered
            for (int j=delivery_day[i]+1; j<inst.T; j++) 
                for (int v=0; v<inst.N; v++) 
                    sol.latest_visit[v][j] = delivery_day[i];
            for (int j=delivery_day[i]; j<inst.T; j++)
                for (int v=0; v<inst.N; v++)
                    sol.t_hat[v][j] = inst.T-1;

            /*
            double demand_per_period=0;
            for (int j=delivery_day[i]; j<inst.T; j++) {
                demand_per_period += demand_per_day[j];
                if (j!=delivery_day[i])
                    sol.slack_capacity[j] = inst.num_trucks*inst.truck_capacity;
            }
            sol.slack_capacity[delivery_day[i]] = inst.num_trucks*inst.truck_capacity-demand_per_period; 
            */
            }
        }
    
    dumbUpdateSlack(inst, sol);

    if (SANITY_CHECK) {
        DEBUG_MSG("Start sanity check for slack_capacity...");
        for (int t=0; t<inst.T; t++)
            assert(sol.slack_capacity[t] >= 0);
        DEBUG_MSG("Sanity check passed!");
    }
    
    // holding cost is computed with the definition that delivery which takes place 
    // on day t includes demand from day t to day t_hat[v][t]. Note this is inconsistent
    // with the definition of latest_visit[v][t] (since latest_visit != t on a delivery 
    // day t) but the def of latest_visit makes it easier to self-update and calculate 
    // savings etc., and does not cause other inconsistencies throughtout the code
    sol.total_holding = getTotalHolding(inst, sol);
}

void dumbUpdateSlack(const Inst& inst, Solution& sol) 
{
    // compute slack matrix
    for (int t=0; t<inst.T; t++) {
        vector<double> demand(inst.N, 0);
        double slack = inst.num_trucks*inst.truck_capacity;
        getDemandToDeliver(demand, inst, sol, t);
        
        for (int v=0; v<inst.N; v++)
            if (sol.visit_set[t].find(v) != sol.visit_set[t].end())
                slack -= demand[v];
        sol.slack_capacity[t] = slack;
        /*
        if (t==3) {
            cout << "On day 3: " << endl;
            for (int v=0; v<inst.N; v++)
                cout << demand[v] << " ";
            cout << "Slack for day 3: " << sol.slack_capacity[t] << endl;
        }
        */
        /*
        cout << "Demand on day " << t << " is: " << 
            inst.num_trucks*inst.truck_capacity - slack << endl;
        */
    } 
}

void localSearch(const Inst& inst, Solution& sol, const double ter, const int max_iter, double& time)
{
    double improve_ratio = 1;
    int counter = 0;
    cout <<"Start local search." << endl;
    auto start = chrono::steady_clock::now();
    while (improve_ratio >= ter && counter <= max_iter) {
        cout << endl;
        cout << "Round " << counter << " starts..." << endl;
        vector<LocalSearchResult> vec_result;;
        for (int operation=kAddWithNoProxy; operation<=kDelWithNoProxy;operation++) {
            // looping through enum, need to cast it back to enum (bad practice!)
            cout << "Search via operation " << operation << endl;
            vec_result.push_back(localSearchPerOperation(static_cast<LocalSearchOperation>(operation), inst, sol));
            cout << "Search via operation " << operation << " finished." << endl;
        }
        size_t which_best = 0;
        LocalSearchResult best = pickBestResult(sol, vec_result, improve_ratio, which_best);
        updateSol(sol, best, inst);
        cout << "Round " << counter << " finishes." << endl;
        cout << "Best day: " << best.day << ". Best operation: " << which_best << endl;
        cout << "Improvement ratio: " << improve_ratio << endl;
        //cout << "After, slack on day " << best.day << ": " << sol.slack_capacity[best.day] << endl;
        

        if (SANITY_CHECK) {
            checkSolCorrectness(inst, sol);
            // check if slack matrix is consistent with demand_to_deliver
            for (int t=0; t<inst.T; t++) {
                vector<double> demand(inst.N, 0);
                double slack = inst.num_trucks*inst.truck_capacity;
                getDemandToDeliver(demand, inst, sol, t);
                for (int v=0; v<inst.N; v++)
                    if (sol.visit_set[t].find(v) != sol.visit_set[t].end())
                        slack -= demand[v];
                assert(abs(slack - sol.slack_capacity[t]) < 1e-6);
                assert(sol.slack_capacity[t] >= -1e-6);
            } 
        }
        ++counter;
    }
    auto end = chrono::steady_clock::now();
    auto duration = duration_cast<microseconds>(end-start);
    time = (double)duration.count()*1e-6;
    cout << "Local search terminates in " << counter << " steps." << endl;
    cout << "Routing cost..." << endl;
    for (int t=0; t<inst.T; t++) {
        cout << "Day " << t << ": " << sol.routing_per_day[t] << endl;
        cout << "    size: " << sol.visit_set[t].size() << endl;
    }
    cout << "Total cost = " << getTotalCost(sol) <<endl; 
}

LocalSearchResult localSearchPerOperation(const LocalSearchOperation operation, const Inst& inst, const Solution& sol)
{
    // This function computes one step of local search based on operation type and returns the one with the most saving in PCST cost
    vector<LocalSearchResult> vec_result;
    // local search starts on the second day (day 1) because everybody needs, add dummy result to vec_result
    LocalSearchResult dummy_result;
    dummy_result.day = 0;
    dummy_result.pcst_visit = sol.visit_set[0];
    dummy_result.routing = sol.routing_per_day[0];
    dummy_result.holding_difference = 0;
    vec_result.push_back(dummy_result);
    // to be visited on the first day
    for (int s=1; s<inst.T; s++) {
        //cout << "Day " << s << endl;
        cout << "Start search on day " << s << endl;
        vec_result.push_back(searchOnDay(operation, inst, sol, s));
        cout << "Finish search on day " << s << endl;
    }
    double improve_ratio = 0;
    size_t which_best = 0;
    return pickBestResult(sol, vec_result, improve_ratio, which_best);
}

LocalSearchResult pickBestResult(const Solution& sol, const vector<LocalSearchResult>& vec_result, double& improve_ratio, size_t& which_best) 
{
    // This function returns the best result among vec_result
    // w.r.t. the sum of holding and routing cost
    double best_change = 0;
    LocalSearchResult best_result = vec_result[0];
    for (size_t i=0; i<vec_result.size(); i++) {
        LocalSearchResult result = vec_result[i];
        double change = result.holding_difference + result.routing - sol.routing_per_day[result.day];
        if (change < best_change) {
            best_result = result;
            best_change = change;
            which_best = i;
        }
    }
    //cout << "Operation " << ops << " is the best." << endl;
    double total_cost_old = getTotalCost(sol);
    improve_ratio = -best_change/total_cost_old;
    return best_result;
} 


// This function computes the result after applying one step of local search on day [day]
LocalSearchResult searchOnDay(const LocalSearchOperation operation, const Inst& inst, const Solution& sol, const int day)
{
    // in capacitated case, naive add and delete does not preserve feasibility:
    // by feasibility, I mean whether total demand to deliver <= total capacity
    // adding too many nodes may make the current day infeasible
    // deleting too many nodes may make previous visiting days infeasibile
    // We maintain feasibility by augmenting knapsack constraint (for each relevant day) 
    // to the objective function of PCST. We do this by manually initialize multiplier weights and update them in a heuristic fashion (i.e., if there is infeasibility, then we increase the weight multiplicatively.
    // original penalty (i.e. saving for visiting nodes)
    vector<double> test_penalty = getPenalty(operation, inst, sol, day);
    unordered_set<int> test_visit;
    double test_routing_cost = 0;
    getPCST(test_visit, test_routing_cost, inst, test_penalty);
    LocalSearchResult test_result = getResultFromVisit(test_visit, test_routing_cost, day, inst, sol);
    // use a temp sol to check if the new solution is feasible
    // BUG here TBD!
    vector<double> slack = getSlackFromNewVisit(test_visit, inst, sol, day);
    if (isFeasible(slack))
        return test_result;

    cout << "Found infeasible day: ";
    for (int t=0; t<inst.T; t++)
        if (slack[t] < 1e-6)
            cout << "Day " << t << ", slack:  " << slack[t] << endl;
    cout << "Start Lagrangian heuristics for operation " << operation << "..." << endl;
    Visit visit;
    double routing_cost=0;
    getVisitAndRoutingCostFromLagRelax(visit, routing_cost, inst, sol, day, test_penalty, operation);
    cout << "On day  " << day << ", visit size after Lagrangian: " << visit.size() <<", routing: " << routing_cost << endl;
    if (SANITY_CHECK) {
        vector<double> slack = getSlackFromNewVisit(visit, inst, sol, day);
        for (size_t i=0; i<slack.size(); i++) {
            cout << "Day " << i << ", slack = " << slack[i] << endl;
            assert(slack[i]>=-1e-6);
        }
    }
    auto result = getResultFromVisit(visit, routing_cost, day, inst, sol);
    cout << "Lagrangian finishes." << endl;
    return result;
}

void getVisitAndRoutingCostFromLagRelax(Visit& visit, double& routing_cost, const Inst& inst, const Solution& sol, const int day, const vector<double> penalty, const LocalSearchOperation operation)
{
    if (operation == kAddWithNoProxy) {
        LagForAdd(visit, routing_cost, inst, sol, day, penalty);
    }
    if (operation == kDelWithNoProxy) {
        LagForDel(visit, routing_cost, inst, sol, day, penalty);
    }
    vector<double> slack = getSlackFromNewVisit(visit, inst, sol, day);
    if (!isFeasible(slack)) {
        visit = sol.visit_set[day];
        routing_cost = sol.routing_per_day[day];
    }
}

LocalSearchResult getResultFromVisit(const Visit& visit, const double routing_cost, const int day, const Inst& inst, const Solution& sol) 
{
    LocalSearchResult result;
    result.day = day;
    result.pcst_visit = visit;
    result.routing = routing_cost;
    result.holding_difference = getHoldingDifference(inst, sol, day, visit);
    return result;
}

double getUbForAdd(const Inst& inst, const Solution& sol, const vector<double>& penalty, const int day)
{
    // PCST value where no new node is added on *day*
    double val = sol.routing_per_day[day];
    // get all penalties for non-added nodes
    for (int v=0; v<inst.N; v++)
        if (sol.visit_set[day].find(v) == sol.visit_set[day].end())
            val += penalty[v];
    return val;
}

void LagForAdd(Visit& visit, double& routing_cost, const Inst& inst, const Solution& sol, const int day, const vector<double>& penalty)
{
    // uses subgradient method to solve lagrangian relaxation
    vector<double> demand(inst.N, 0);
    getDemandToDeliver(demand, inst, sol, day);
    double multiplier = 0;
    double eps(2);
    double old_lag_val = 0;
    double lag_val = 0;
    // compute upper bound (where no node is added)
    double ub = getUbForAdd(inst, sol, penalty, day); 
    // virtual_penalty is the penalty after modification from dualizing knapsack
    vector<double> virtual_penalty(penalty.begin(), penalty.end());
    int count = 0;
    // start subgradient method
    while (count < kMaxIter) {
        // compute subgradient
        // first compute x, i.e. set of visits given virtual_penalty
        count++;
        getPCST(visit, routing_cost, inst, virtual_penalty);
        // use x to compute grad for multiplier
        double grad = getGradForAdd(demand, visit, sol, day);
        if (grad <= 0) 
            break;
        lag_val = getLagValForAdd(visit, inst, penalty, routing_cost, multiplier, grad);
        // half eps if lagrangian bound is not increasing significantly (over-shoot)
        if (abs(lag_val - old_lag_val)/lag_val <= kThreshLagProgress)
            eps /= 2;
        old_lag_val = lag_val;
        // Held et. al. heristic on stepsize 
        double stepsize = eps*(ub-lag_val)/(grad*grad);
        // update multiplier via subgrad assent 
        multiplier += stepsize * grad; 
        // given the new multiplier, update virtual_penalty
        updateVirtualPenaltyForAdd(sol.visit_set[day], virtual_penalty, multiplier, demand); 
       if (count == kMaxIter) 
           cout << "Exceed iter limit. Terminate." << endl;
    }
}

void updateVirtualPenaltyForAdd(const Visit& visit, vector<double>& penalty, const double multiplier, const vector<double>& demand)
{
    for (size_t v=0; v<penalty.size(); v++)
        if (visit.find(v) == visit.end()) {
            penalty[v] -= multiplier*demand[v];
            if (penalty[v] < 0)
                penalty[v] = 0;
        }
}

double getGradForAdd(const vector<double>& demand, const Visit& visit, const Solution& sol, const int day)
{
    // grad = Ax-b. In add, it's one knapsack w.r.t. today
    double grad = -sol.slack_capacity[day];
    // knapsack involves those that are not visited on *day* but included in visit
    for (auto const& v:getSetDifference(visit, sol.visit_set[day]))
            grad += demand[v];
    return grad;
}

double getLagValForAdd(const Visit& visit, const Inst& inst, const vector<double>& penalty, const double routing_cost, const double multiplier, const double grad)
{
    // value = routing_cost + penalty on unvisited nodes + dualized knapsack constraint
    double val = routing_cost;
    for (int v=0; v<inst.N; v++) 
        if (visit.find(v) == visit.end()) 
            val += penalty[v];
    val += multiplier * grad;
    return val;
}

void LagForDel(Visit& visit, double& routing_cost, const Inst& inst, const Solution& sol, const int day, const vector<double>& penalty)
{
    vector<double> demand(inst.N, 0);
    getDemandToDeliver(demand, inst, sol, day);
    map<int, unordered_set<int> > node_partition;
    getNodePartition(node_partition, sol, inst, day, demand);  // segmentation fault happens here!
    map<int, double> multiplier;
    for (auto const& v:node_partition)
        multiplier[v.first] = 0;

    double eps(2);
    // compute upper bound: no node is deleted incurring zero penalty
    double ub = sol.routing_per_day[day];
    double old_lag_val = 0;
    double lag_val = 0;
    // virtual_penalty is the penalty after modification from dualizing knapsack
    vector<double> virtual_penalty(penalty.begin(), penalty.end());
    int count = 0;
    // start subgradient method
    while (count < kMaxIter) {
        // compute subgradient
        // first compute x
        count++;
        getPCST(visit, routing_cost, inst, virtual_penalty);
        map<int, double> grad = getGradForDel(demand, visit, sol, node_partition);
        // if all entries of grad are non-positive, feasibility is attained
        bool is_feasible = true;
        for (auto const& v:grad)
            if (v.second>0) is_feasible = false;
        if (is_feasible) 
            break;
        lag_val = getLagValForDel(visit, sol, day, penalty, routing_cost, multiplier, grad);
        if (abs(lag_val-old_lag_val)/lag_val <= kThreshLagProgress) 
            eps /= 2;
        old_lag_val = lag_val;
        double stepsize = getStepsizeForDel(ub, lag_val, eps, grad);
        // subgrad assent for multiplier
        for (auto& v:multiplier) {
            /*
            if (grad[v.first] < 0)
                continue;
            */
            v.second += stepsize * grad[v.first];
        }
       updateVirtualPenaltyForDel(virtual_penalty, multiplier, demand, sol, day);
       if (count == kMaxIter) 
           cout << "Exceed iter limit. Terminate." << endl;
    }
    // if still not feasible, don't make any change in the current day!
    // feasibilityFix(visit, routing_cost, blah)
}

void updateVirtualPenaltyForDel(vector<double>& virtual_penalty, const map<int, double>& multiplier, const vector<double>& demand, const Solution& sol, const int day)
{
    // increase penalty only on visited nodes, since we only delete from visited nodes
    for (auto const& v:sol.visit_set[day]) {
    // find the right multiplier
        int latest_visit = sol.latest_visit[v][day];
        auto const& it = multiplier.find(latest_visit);
        if (it != multiplier.end())
            virtual_penalty[v] += it->second*demand[v];
    }
}


double getStepsizeForDel(const double ub, const double lag_val, const double eps, const map<int, double>& grad)
{
    double denominator = 0;
    for (auto const& v:grad) denominator+= v.second*v.second;
    return eps*(ub-lag_val)/denominator;
}

map<int, double> getGradForDel(const vector<double>& demand, const Visit& visit, const Solution& sol, const map<int, unordered_set<int> >& node_partition)
{
    // each entry of grad corresponds to one entry of node_partition
    // recall <key, value> of node_partition: key: latest visit, value: set of nodes whose latest_visit is key
    map<int, double> grad;
    for (auto const& elem:node_partition) {
        grad[elem.first] = -sol.slack_capacity[elem.first];
        for (auto const& v:elem.second) 
            // if deleted 
            if (visit.find(v) == visit.end())
                grad[elem.first] += demand[v];
    }
    return grad;
}

double getLagValForDel(const Visit& visit, const Solution& sol, const int day, const vector<double>& penalty, const double routing_cost, const map<int, double>& multiplier, const map<int, double>& grad)
{
    // value = routing_cost + penalty on deleted nodes + dualized knapsack constraint
    double val = routing_cost;
    for (auto const& v:getSetDifference(sol.visit_set[day], visit))
    /*
    for (int v=0; v<inst.N; v++) 
        if (visit.find(v) == visit.end()) 
    */
        val += penalty[v];
    for (auto const& v:multiplier) { 
        auto const& it = grad.find(v.first);
        val += v.second * it->second;
    }
    return val;
}
        
/*

    // o/w use lagrangian multiplier
    // for add, there is only one knapsack constraint w.r.t. current day
    // for delete, there are a few knapsack constraints for past days
    vector<double> demand_per_node(inst.N, 0); 
    getDemandToDeliver(demand_per_node, inst, sol, day);
    // partition visited nodes by their latest_visit
    map<int, unordered_set<int> > node_partition;
    getNodePartition(node_partition, sol, inst, day, demand_per_node);
    // demand for nodes that are not visited today (used for add operation)
    vector<double> demand_unvisited_nodes(inst.N, 0);
    for (int v=0; v<inst.N; v++) 
        if (sol.visit_set[day].find(v) == sol.visit_set[day].end())
            demand_unvisited_nodes[v] = demand_per_node[v];
    if (SANITY_CHECK) {
        if (operation == kAddWithNoProxy) {
            double demand_unvisited = 0;
            for (int v=0; v<inst.N; v++)
                demand_unvisited += demand_unvisited_nodes[v];
            assert(demand_unvisited > sol.slack_capacity[day]);
        }
    }
    
    vector<double> penalty(inst.N, 0); 
    if (operation == kAddWithNoProxy) {
        double ub = getUbForAdd(inst, sol, demand_per_node, test_penalty, day);
        penalty = binarySearchForPenalty(inst, sol, demand_per_node, ub, day);
    } 
    if (operation == kDelWithNoProxy) {
        double ub = getUbForDel(inst, sol, demand_per_node, node_partition, test_penalty, day);
        penalty = binarySearchForPenalty(inst, sol, node_partition, ub, day);
    } 
    unordered_set<int> visit;
    double routing_cost = 0;
    getPCST(visit, routing_cost, inst, penalty);
    LocalSearchResult result;
    result.day = day;
    result.pcst_visit = visit;
    result.routing = routing_cost;
    result.holding_difference = getHoldingDifference(inst, sol, day, visit);
    return result;

double getUbForAdd(const Inst& inst, const Solution& sol, const vector<double>& demand_per_node, const vector<double>& test_penalty, const int day, double guess)
{
    vector<double> new_penalty(inst.N, 0);  
    getNewPenaltyForAdd(new_penalty, inst, sol.visit_set[day], test_penalty, demand_per_node, guess);
    while (true) {
        getNewPenaltyForAdd(new_penalty, inst, sol.visit_set[day], test_penalty, demand_per_node, guess);
        if (isFeasiblePenaltyForDel(new_penalty))
            break;
        guess *= 2;
    }
    // compute value from new_penalty
}

double getUbForDel(const Inst& inst, const Solution& sol, const vector<double>& demand_per_node, const map<int, unordered_set<int> >& node_partition, const vector<double>& test_penalty, const int day, double guess);

vector<double> getNewPenaltyForAdd(const Inst& inst, const Visit& visit, const vector<double>& penalty, const vector<double>& demand, const double guess)
{
    vector<double> new_penalty(inst.N, 0);
    for (int v=0; v<inst.N; v++)
        if (visit.find(v) == visit.end()) {
            new_penalty[v] = penalty[v] - demand[v]*guess;
            if (new_penalty[v] < 0) 
                new_penalty[v] = 0;
        }
    return new_penalty;
}

bool isFeasiblePenaltyForAdd(const Inst& inst, const Solution& sol, const vector<double>& penalty, const vector<double>& demand_per_node, const int day, const double ub)
{
    Visit visit;
    double routing_cost = 0;
    getPCST(visit, routing_cost, inst, new_penalty);
    vector<double> slack = getSlackFromNewVisit(visit, inst, sol, day);
    if (SANITY_CHECK) 
        for (int t=0; t<inst.T && t != day; t++)
            assert(slack[t] >= 0);
    if (slack[day] < 0)
        return false;
    return true;
}
*/

// generate a slack vector after replacing the old visit on day *day$ of solution by visit
vector<double> getSlackFromNewVisit(const Visit& visit, const Inst& inst, const Solution& sol, const int day) 
{
    // dumb implementation of computing slack because i don't know what's wrong 
    // with the smart version!!!
    
    // dumb copy a solution
    Solution sol_tmp = sol;
    /*
    sol_tmp.visit_set = sol.visit_set;
    sol_tmp.total_holding = sol.total_holding;
    sol_tmp.routing_per_day = sol.routing_per_day;
    sol_tmp.latest_visit = sol.latest_visit;
    sol_tmp.t_hat = sol.t_hat;
    sol_tmp.slack_capacity = sol.slack_capacity;
    */
    // routing_cost (2nd arg does not matter, so set it fakely to 0)
    LocalSearchResult result_tmp = getResultFromVisit(visit, 0, day, inst, sol);
    /*
    auto const& visit_add = getSetDifference(visit, sol_tmp.visit_set[day]);
    auto const& visit_del = getSetDifference(sol_tmp.visit_set[day], visit);
    for (auto const& v: sol_tmp.visit_set[day])
        cout << v << endl;
    cout << "Newly added: " << endl;
    for (auto const& v:visit)
        cout << v << endl;
    */
    updateSol(sol_tmp, result_tmp, inst);
    return sol_tmp.slack_capacity;
    /*
    // update latest_visit and t_hat
    Visit visit_added = getSetDifference(visit, sol.visit_set[day]);
    Visit visit_deleted = getSetDifference(sol.visit_set[day], visit);

    // update slack matrix
    vector<double> demand_per_node(inst.N, 0);
    getDemandToDeliver(demand_per_node, inst, sol, day);
    vector<double> slack(sol.slack_capacity.begin(), sol.slack_capacity.end());
    // for nodes that are added, increase slack in their latest day and decrease slack today
    for (auto pt=visit_added.begin(); pt!=visit_added.end(); pt++) {
        int latest_visit = sol.latest_visit[*pt][day];
        slack[latest_visit] += demand_per_node[*pt];
        slack[day] -= demand_per_node[*pt];
    }

    // for nodes that are deleted, track its latest visit
    for (auto pt=visit_deleted.begin(); pt!=visit_deleted.end(); pt++) {
        int latest_visit = sol.latest_visit[*pt][day];
        slack[latest_visit] -= demand_per_node[*pt];
        slack[day] += demand_per_node[*pt];
    }
    */
}

//bool isFeasibleUbForDel(const Inst& inst, const Solution& sol, const vector<double>& penalty, const vector<double>& demand_per_node, const int day, const double ub);

        
/*
vector<double> binarySearchForPenalty(const Inst& inst, const Solution& sol, const vector<double>& demand_unvisited_nodes, double ub)
{
}

vector<double> binarySearchForPenalty(const Inst& inst, const Solution& sol, const map<int, unordered_set<int> >& node_partition, double ub)
{
}
*/

double getHoldingDifference(const Inst& inst, const Solution& sol, const int day, const unordered_set<int>& new_visit)
{
    double holding_difference = 0;
    // This function computes the difference of holding cost via changing from old visit to new visit
    const unordered_set<int>& old_visit = sol.visit_set[day];
    // for v in old_visit - new_visit, our holding cost goes up by the amount of saving
    // for v in new_visit - old_visit, our holding cost goes down by the amount of saving
    Visit old_minus_new = getSetDifference(old_visit, new_visit);
    Visit new_minus_old = getSetDifference(new_visit, old_visit);
    for (auto v=old_minus_new.begin(); v!=old_minus_new.end(); v++)
        holding_difference += getSaving(inst, sol, day, *v);
    for (auto v=new_minus_old.begin(); v!=new_minus_old.end(); v++)
        holding_difference -= getSaving(inst, sol, day, *v);
    return holding_difference;
}

template<typename T> unordered_set<T> getSetDifference(const unordered_set<T>& s1, const unordered_set<T>& s2)
{
    // This function computes s1-s2
    unordered_set<T> diff;
    for (auto v=s1.begin(); v!=s1.end(); v++)
        if (s2.find(*v) == s2.end())
            diff.insert(*v);
    return diff;
}

void getDemandToDeliver(vector<double>& demand_per_node, const Inst& inst, const Solution& sol, const int day)
{
    // Constructs demand matrix where demand_per_node[v] is the amount to deliver on day *day* if v is visited
    for (int v=0; v<inst.N; v++) {
        double demand=0;
        for (int t=day; t<=sol.t_hat[v][day]; t++)
            demand += inst.demand[v][t];
        demand_per_node[v] = demand;
    }
}

void getNodePartition(map<int, unordered_set<int> >& node_partition, const Solution& sol, const Inst& inst, const int day, const vector<double>& demand_per_node)
{
    cout << "Getting node partition for day " << day << endl;
    // for each node visited on day *day* , find its latest_visit and insert it into node_partition
    for (int v=0; v<inst.N; v++) {
        if (sol.visit_set[day].find(v) == sol.visit_set[day].end())
            continue;
        int latest_visit = sol.latest_visit[v][day];
        if (node_partition.find(latest_visit) == node_partition.end()) {
            unordered_set<int> S;
            S.insert(v);
            node_partition.insert(pair<int, unordered_set<int> >(latest_visit, S));
        }
        auto it2 = node_partition.find(latest_visit);
        it2->second.insert(v);
    }
    cout << "Node partition cardinality: " << node_partition.size() << endl;
    for (auto it=node_partition.cbegin(); it!=node_partition.cend();) {
        // check for each day if the total demand can exceed slack. If not, delete that day 
        double demand = 0;
        cout << "Day " << it->first << " has clients: ";
        for (auto const& v : it->second) {
            cout << v << ", ";
            demand += demand_per_node[v];
        }
        cout << "On day " << it->first << " demand: " <<
            demand << ", slack " << sol.slack_capacity[it->first] << endl;
        if (demand <= sol.slack_capacity[it->first]) { 
            cout << "Constraint always inactive, deleting day " << it->first << endl;
            node_partition.erase(it++);
        }
        else
            ++it;
    }
    // segmentation fault here!
}

vector<double> getPenalty(LocalSearchOperation operation, const Inst& inst, const Solution& sol, const int day)
{
    /*
    // demand to deliver for each node if they are selected
    vector<double> demand_per_node(inst.N, 0); 
    getDemandToDeliver(demand_per_node, inst, sol, day);
    // partition demand_per_node by their latest_visit
    map<int, unordered_set<int> > node_partition;
    getNodePartition(node_partition, sol, inst, day);

    DEBUG_MSG("Starting sanity check for node partition function...");
    if (SANITY_CHECK) {
        for (int v=0; v<inst.N; v++) {
            if (sol.visit_set[day].find(v) != sol.visit_set[day].end()) {
                int latest_visit = sol.latest_visit[v][day];
                auto it2 = node_partition.find(latest_visit);
                assert(it2 != node_partition.end());
                assert(it2->second.find(v)!=it2->second.end());
            }
        }
    }
    DEBUG_MSG("Sanity check passed!");
    */
    int largePenalty = 0;
    for (int i=0; i<inst.N; i++)
        for (int j=0; j<inst.N; j++)
            largePenalty += inst.w[i][j];
    vector<double> penalty(inst.N, 0);
    int non_positive_penalty = 0;
    unordered_set<int> visit = sol.visit_set[day];
    // MUST include depot 0
    penalty[0] = inst.large_penalty;

    for (int v=1; v<inst.N; v++) {
        double saving = getSaving(inst, sol, day, v);
        //DEBUG_MSG("Saving: " << saving << ", proxy: " << cvpr_proxy << endl);
        switch(operation)
        {
            case kEdit: {
        // in edit case, we do not care if a node is visited on that day, the penalty of node v is saving - CVRP_proxy
                penalty[v] = saving;
                break;
                        }
            case kDelWithNoProxy: 
            {
                if (visit.find(v) == visit.end()) {
                    penalty[v] = 0;
                }
                else {
                    penalty[v] = saving;
                }
                break;
            }
            case kAddWithNoProxy:
            {
                if (visit.find(v) == visit.end())
                    penalty[v] = saving;
                else
                    penalty[v] = largePenalty;
                break;
            }
        }
        if (penalty[v] <= 0) 
            ++non_positive_penalty;
        
    }
    DEBUG_MSG("Day " << day << ", operation " << operation << ". Non-positive penalty percentage: " << (double)non_positive_penalty/(double)inst.N );
    return penalty;
}
            
// This functtion computes the saving when adding a visit on [day] to node v
double getSaving(const Inst& inst, const Solution& sol, const int day, const int v)
{
    // fix s, saving = \sum_{t>=s}^{t_hat[v][s]}(H[v][l][t] - H[v][s][t]) where l is the latest_visit[v][s]
    double saving = 0;
    int l = sol.latest_visit[v][day];
    for (int t=day; t<=sol.t_hat[v][day]; t++) 
       saving += inst.H[v][l][t] - inst.H[v][day][t]; 
    return saving;
}

double getCVPR_Proxy(const Inst& inst, const Solution& sol, const int day, const int v)
{
    // proxy = 2*w[0][v]* \sum_{t>=day}^{t_hat[v][t]}demand[v][t]/capacity
    double demand_for_the_period = 0;
    for (int i=day; i<= sol.t_hat[v][day]; i++)
        demand_for_the_period += inst.demand[v][i]; 
    return 2*inst.w[0][v] * demand_for_the_period / inst.truck_capacity;
}

void updateSol(Solution& sol, const LocalSearchResult& result, const Inst& inst)
{
    // update latest_visit and t_hat
    Visit visit_added = getSetDifference(result.pcst_visit, sol.visit_set[result.day]);
    //cout << "After call to getPenalty" << endl;
    Visit visit_deleted = getSetDifference(sol.visit_set[result.day], result.pcst_visit);
    // for a newly added node v, its latest_visit[v][t]=day where day+1 <= t <= t_hat+1
    // t_hat[v][t]=day-1 where latest_visit[v][day] <= t <= day-1
    for (auto pt=visit_added.begin(); pt!=visit_added.end(); pt++) {
        if (result.day+1 < inst.T) { 
            for (int t=result.day+1; t<=sol.t_hat[*pt][result.day]+1; t++)
                sol.latest_visit[*pt][t] = result.day;
        }
        for (int t=sol.latest_visit[*pt][result.day]+1; t<=result.day-1; t++)
            sol.t_hat[*pt][t] = result.day-1;
    }

    // for a newly deleted v, its latest_visit[v][t]=sol.latest_visit[v][day] where day+1 <= t <= t_hat+1
    // t_hat[v][t]=sol.t_hat[v][day] where latest_visit[v][day] <= t <= day-1
    for (auto pt=visit_deleted.begin(); pt!=visit_deleted.end(); pt++) {
        if (result.day+1 < inst.T) {
            for (int t=result.day+1; t<=sol.t_hat[*pt][result.day]+1; t++)
                sol.latest_visit[*pt][t] = sol.latest_visit[*pt][result.day];
        }
        for (int t=sol.latest_visit[*pt][result.day]; t<=result.day-1; t++)
            sol.t_hat[*pt][t] = sol.t_hat[*pt][result.day];
    }

    sol.routing_per_day[result.day] = result.routing;
    sol.total_holding += result.holding_difference;
    sol.visit_set[result.day] = result.pcst_visit;
    dumbUpdateSlack(inst, sol);

    /*
    // smart update slack matrix
    vector<double> demand_per_node(inst.N, 0);
    getDemandToDeliver(demand_per_node, inst, sol, result.day);
    // for nodes that are added, increase slack in their latest day and decrease slack today
    for (auto pt=visit_added.begin(); pt!=visit_added.end(); pt++) {
        int latest_visit = sol.latest_visit[*pt][result.day];
        sol.slack_capacity[latest_visit] += demand_per_node[*pt];
        sol.slack_capacity[result.day] -= demand_per_node[*pt];
    }

    // for nodes that are deleted, track its latest visit
    for (auto pt=visit_deleted.begin(); pt!=visit_deleted.end(); pt++) {
        int latest_visit = sol.latest_visit[*pt][result.day];
        sol.slack_capacity[latest_visit] -= demand_per_node[*pt];
        sol.slack_capacity[result.day] += demand_per_node[*pt];
    }
    */

    //cout << "Checking feasibility after updating the solution..." << endl;
    /*
    for (int t=0; t<inst.T; t++)
        cout << "Slack on day " << t << ": " << sol.slack_capacity[t];
    */
    // assert feasibility
    //assert(isFeasible(sol.slack_capacity));
    //cout << "Feasibility check after updating solution passed!" << endl;
    
    // update remaining info
}

bool isFeasible(const vector<double>& slack) 
{
    for (size_t t=0; t<slack.size(); t++)
        if (slack[t] < 0)
            return false;
    return true;
}
