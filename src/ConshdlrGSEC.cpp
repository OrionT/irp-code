/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                  This file is part of the program and library             */
/*         SCIP --- Solving Constraint Integer Programs                      */
/*                                                                           */
/*    Copyright (C) 2002-2019 Konrad-Zuse-Zentrum                            */
/*                            fuer Informationstechnik Berlin                */
/*                                                                           */
/*  SCIP is distributed under the terms of the ZIB Academic License.         */
/*                                                                           */
/*  You should have received a copy of the ZIB Academic License.             */
/*  along with SCIP; see the file COPYING. If not visit scip.zib.de.         */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   ConshdlrSubtour.cpp
 * @brief  Subtour elimination constraint handler for TSP problems, written in C++
 * @author Timo Berthold
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/
// std include
#include <cassert>
#include <string>
#include <iostream>
#include <numeric>

// scip include
#include "objscip/objscip.h"
#include "scip/cons_linear.h"

// boost graph library include
#include <boost/graph/boykov_kolmogorov_max_flow.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>

// user-defined include
#include "ConshdlrGSEC.h"

using namespace tsp;
using namespace scip;
using namespace std;
using namespace boost;

#define RC_EPS 1e-4
//#define DEBUG_CONSHDLR
//#define DEBUG_SUBTOUR
#define SCIP_DEBUG

struct SCIP_ConsData
{
   vector< vector< SCIP_VAR* > >* x;
   vector< SCIP_VAR* >* z;
};

// -------------------------
// Methods used in callbacks
// -------------------------

static
SCIP_Bool findSubtour(
        SCIP* scip, 
        SCIP_CONSDATA* consdata,
        SCIP_SOL* sol
        )
{
    assert( consdata != NULL );
    auto& x = *(consdata->x);
    auto& z = *(consdata->z);
    int n = x.size();
    
    // get LP values for x, z variables
    vector< vector< double > > xval(n);
    vector< double > zval(n);
    for (int i=0; i<n; i++) 
    { 
        assert( z[i] != NULL );
        zval[i] = SCIPgetSolVal(scip, sol, z[i]);
        xval[i].resize(i);
        for (int j=0; j<i; j++)
        {
            assert( x[i][j] != NULL );
            xval[i][j] = SCIPgetSolVal(scip, sol, x[i][j]);
            /*
            char debug_msg_conshdlr[255];
            sprintf(debug_msg_conshdlr, "(%d, %d): %f", i, j, xval[i][j]);
            std::cout << debug_msg_conshdlr << std::endl;
            std::cout << xval[i][j] << ", ";
            */
        }
        //std::cout << std::endl;
    }

    // init boost graph
    Graph g(n);
    property_map < Graph, edge_capacity_t >::type 
        capacity = get(edge_capacity, g);
    property_map < Graph, edge_residual_capacity_t >::type
        residual_capacity = get(edge_residual_capacity, g);
    property_map < Graph, edge_reverse_t >::type 
        rev = get(edge_reverse, g);
    property_map < Graph, vertex_color_t >::type
        color = get(vertex_color, g);

    // set arc/anti-arc weights (required by kolmogorov max flow)
    Traits::edge_descriptor e1, e2;
    bool inserted;
    for (int i=0; i<n; i++)
        for (int j=0; j<i; j++) {
            if (abs(xval[i][j]) > RC_EPS) {
                tie(e1, inserted) = add_edge(i,j,g);
                assert(inserted);
                capacity[e1] = xval[i][j];
                tie(e2, inserted) = add_edge(j,i,g);
                assert(inserted);
                capacity[e2] = xval[i][j];
                rev[e1] = e2;
                rev[e2] = e1;
            }
        }

    // find the largest zval idx, fix that idx and iterate over other nodes
    auto idx = sort_indices(zval, true);
    int ztop = idx[0]; // node with highest z value, need to separate it with every other guys
    assert(ztop == 0);
    double flow;
            char debug_msg_conshdlr[255];
    for (int h=0; h<n; h++) {
        if (h != ztop)
        {
            flow = boykov_kolmogorov_max_flow(g, h, ztop);
            if (flow <= 2.0*(zval[h] + zval[ztop] - 1.0) - RC_EPS)
            {
#ifdef DEBUG_SUBTOUR
            sprintf( debug_msg_conshdlr, "Two nodes: %d, %d, flow: %f, node sum: %f", ztop, h, flow, 2.0*(zval[h]+zval[ztop]-1.0) );
            std::cout << debug_msg_conshdlr << std::endl;
#endif
                return true;
            }
        }
    }
    return false;
}

/* separates subtour elemination cuts */
static
SCIP_RETCODE sepaSubtour(
   SCIP*              scip,               /**< SCIP data structure */
   SCIP_CONSHDLR*     conshdlr,           /**< the constraint handler itself */
   SCIP_CONS**        conss,              /**< array of constraints to process */
   int                nconss,             /**< number of constraints to process */
   int                nusefulconss,       /**< number of useful (non-obsolete) constraints to process */
   SCIP_SOL*          sol,                /**< primal solution that should be separated */
   SCIP_RESULT*       result              /**< pointer to store the result of the separation call */
   )
{
   assert(result != NULL);

   *result = SCIP_DIDNOTFIND;
   for( int c = 0; c < nusefulconss && *result != SCIP_CUTOFF; ++c )
   {
        // get all required structures
        SCIP_CONSDATA* consdata;
        consdata = SCIPconsGetData(conss[c]);
        assert(consdata != NULL);
        auto& x = *(consdata->x);
        auto& z = *(consdata->z);
        assert( x.size() == z.size() );
        int n = x.size();

        vector< vector< double > > xval(n);
        vector< double > zval(n);
        for (int i=0; i<n; i++) { 
            assert( z[i] != NULL );
            zval[i] = SCIPgetSolVal(scip, sol, z[i]);
            xval[i].resize(i);
            for (int j=0; j<i; j++)
            {
                assert( x[i][j] != NULL );
                xval[i][j] = SCIPgetSolVal(scip, sol, x[i][j]);
            }
        }

        // init boost graph
        Graph g(n);
        property_map < Graph, edge_capacity_t >::type 
            capacity = get(edge_capacity, g);
        property_map < Graph, edge_residual_capacity_t >::type
            residual_capacity = get(edge_residual_capacity, g);
        property_map < Graph, edge_reverse_t >::type 
            rev = get(edge_reverse, g);
        property_map < Graph, vertex_color_t >::type
            color = get(vertex_color, g);

        // set arc/anti-arc weights (required by kolmogorov max flow)
        Traits::edge_descriptor e1, e2;
        bool inserted;
        for (int i=0; i<n; i++)
            for (int j=0; j<i; j++) {
                if (abs(xval[i][j]) > RC_EPS) {
                    tie(e1, inserted) = add_edge(i,j,g);
                    assert(inserted);
                    capacity[e1] = xval[i][j];
                    tie(e2, inserted) = add_edge(j,i,g);
                    assert(inserted);
                    capacity[e2] = xval[i][j];
                    rev[e1] = e2;
                    rev[e2] = e1;
                }
            }

        // find the largest zval idx, fix that idx and iterate over other nodes
        auto idx = sort_indices(zval, true);
        int ztop = idx[0]; // node with highest z value, need to separate it with every other guys
        //assert( ztop == 0 );
        *result = SCIP_DIDNOTFIND;

        char debug_msg_conshdlr[255];
        // for each h, find min-cut separating h from depot 0    
        for (int h=0; h<n && *result != SCIP_CUTOFF; h++) 
        {
            if (h == ztop) continue;
            double flow = boykov_kolmogorov_max_flow(g, h, ztop);

            // check if the constraint is violated
            if (flow >= 2.0*(zval[h] + zval[ztop]-1.0) - RC_EPS) continue;

#ifdef DEBUG_SUBTOUR
            sprintf( debug_msg_conshdlr, "In sepa, two nodes: %d, %d, flow: %f, node sum: %f", ztop, h, flow, 2.0*(zval[h]+zval[ztop]-1.0) );
            std::cout << debug_msg_conshdlr << std::endl;
#endif
            // generate cut from two cut sets S and S_bar
            graph_traits < Graph >::vertex_iterator u_iter, u_end;
            vector<Traits::vertex_descriptor> S, S_bar; // S does not contain 0

            for ( tie(u_iter, u_end)=vertices(g); u_iter!=u_end; ++u_iter ) {
                if ((bool)color[*u_iter]) S_bar.push_back(*u_iter);
                else S.push_back(*u_iter);
            }

            auto S_smaller = (S.size() <= S_bar.size() ? S : S_bar);

            // create row (cons) and add vars to it
            SCIP_ROW* row; 
            SCIP_CALL( SCIPcreateEmptyRowCons(
                        scip, 
                        &row, 
                        conshdlr, 
                        "sepa_con", // name
                        -2.0,                 // LHS
                        SCIPinfinity(scip),  // RHS 
                        FALSE,               // is row valid only locally 
                        FALSE,               // is row modifiable s.t. CG 
                        TRUE)                // removeable 
                    ); 


            SCIP_CALL( SCIPcacheRowExtensions(scip, row) );
            for (auto s1 : S)
            {
                for (auto s2 : S_bar)
                {
                    assert( s1 != s2 );
                    SCIP_CALL( SCIPaddVarToRow(scip, 
                                row, (s1 > s2 ? x[s1][s2] : x[s2][s1]), 1.0) ); 
                }
            }
            SCIP_CALL( SCIPaddVarToRow(scip, 
                        row, z[ztop], -2.0) ); 

            SCIP_CALL( SCIPaddVarToRow(scip, 
                        row, z[h], -2.0) ); 



            /*
            SCIP_CALL( SCIPcreateEmptyRowCons(
                        scip, 
                        &row, 
                        conshdlr, 
                        "sepa_con", // name
                        -SCIPinfinity(scip),                 // LHS
                        1.0,  // RHS 
                        FALSE,               // is row valid only locally 
                        FALSE,               // is row modifiable s.t. CG 
                        TRUE)                // removeable 
                    ); 

            SCIP_CALL( SCIPcacheRowExtensions(scip, row) );
            for (int i=0; i<S_smaller.size(); i++) {
                int idx1 = S_smaller[i];
                SCIP_CALL( SCIPaddVarToRow(scip, row, z[idx1], -1.0) ); 
                for (int j=0; j<i; j++) {
                    int idx2 = S_smaller[j];
                    SCIP_CALL( SCIPaddVarToRow(scip, row, (idx1>idx2?x[idx1][idx2] : 
                                    x[idx2][idx1]), 1.0) ); 
                }
            }

            // check if h \in S_smaller 
            if ( S.size() <= S_bar.size() )
            {
                // S_smaller == S in this case, so 0 is not in S_smaller
                assert( S_smaller.find(0) == S_smaller.end() );
                SCIP_CALL( SCIPaddVarToRow(scip, row, z[ztop], 1.0 ) );
                for (auto elem:S_smaller)
                {
                    if (elem != h)
                    {
                        SCIP_CALL( SCIPaddVarToRow(scip, row, z[elem], -1.0) );
                    }
                }
            }
            else
            {
                // S_smaller == S_bar in this case, so 0 is in S_smaller
                assert( S_smaller.find(0) != S_smaller.end() );
                assert( S_smaller.find(h) != S_smaller.end() );
                SCIP_CALL( SCIPaddVarToRow(scip, row, z[h], 1.0 ) );
                for (auto elem:S_smaller)
                {
                    if (elem != 0)
                    {
                        SCIP_CALL( SCIPaddVarToRow(scip, row, z[elem], -1.0) );
                    }
                }
            }
            */

            SCIP_CALL( SCIPflushRowExtensions(scip, row) );
            // add cut when cut is efficacious (see scip_cut.h for definition)
            if( SCIPisCutEfficacious(scip, sol, row) )
            {
               SCIP_Bool infeasible;
               SCIP_CALL( SCIPaddRow(scip, row, FALSE, &infeasible) );
               SCIP_CALL( SCIPreleaseRow(scip, &row) );
               if ( infeasible ) 
               {
                  *result = SCIP_CUTOFF;
#ifdef DEBUG_SUBTOUR
                    sprintf(debug_msg_conshdlr, "CUTOFF: h: %d, flow: %f, RHS: %f",
                            h, flow, 2.0*(zval[h]+zval[0]-1.0) );
                    std::cout << debug_msg_conshdlr << std::endl;
#endif
               }
               else
               {
                  *result = SCIP_SEPARATED;
#ifdef DEBUG_SUBTOUR
                    sprintf(debug_msg_conshdlr, "SEPARATED: h: %d, flow: %f, RHS: %f",
                            h, flow, 2.0*(zval[h]+zval[0]-1.0) );
                    std::cout << debug_msg_conshdlr << std::endl;
#endif
               }
            }
        }
   }
   return SCIP_OKAY;
} /*lint !e715*/


// ----------------------------
// Fundamental callback methods
// ----------------------------

/** constraint enforcing method of constraint handler for LP solutions
 *
 *  The method is called at the end of the node processing loop for a node where the LP was solved.
 *  The LP solution has to be checked for feasibility. If possible, an infeasibility should be resolved by
 *  branching, reducing a variable's domain to exclude the solution or separating the solution with a valid
 *  cutting plane.
 *
 *  The enforcing methods of the active constraint handlers are called in decreasing order of their enforcing
 *  priorities until the first constraint handler returned with the value SCIP_CUTOFF, SCIP_SEPARATED,
 *  SCIP_REDUCEDDOM, SCIP_CONSADDED, or SCIP_BRANCHED.
 *  The integrality constraint handler has an enforcing priority of zero. A constraint handler which can
 *  (or wants) to enforce its constraints only for integral solutions should have a negative enforcing priority
 *  (e.g. the alldiff-constraint can only operate on integral solutions).
 *  A constraint handler which wants to incorporate its own branching strategy even on non-integral
 *  solutions must have an enforcing priority greater than zero (e.g. the SOS-constraint incorporates
 *  SOS-branching on non-integral solutions).
 *
 *  The first nusefulconss constraints are the ones, that are identified to likely be violated. The enforcing
 *  method should process the useful constraints first. The other nconss - nusefulconss constraints should only
 *  be enforced, if no violation was found in the useful constraints.
 *
 *  possible return values for *result (if more than one applies, the first in the list should be used):
 *  - SCIP_CUTOFF     : the node is infeasible in the variable's bounds and can be cut off
 *  - SCIP_CONSADDED  : an additional constraint was generated
 *  - SCIP_REDUCEDDOM : a variable's domain was reduced
 *  - SCIP_SEPARATED  : a cutting plane was generated
 *  - SCIP_BRANCHED   : no changes were made to the problem, but a branching was applied to resolve an infeasibility
 *  - SCIP_INFEASIBLE : at least one constraint is infeasible, but it was not resolved
 *  - SCIP_FEASIBLE   : all constraints of the handler are feasible
 */
SCIP_DECL_CONSENFOLP(ConshdlrSubtour::scip_enfolp)
{
    /*
    ofstream out(output_filename, std::ios::app);
    out << "Sepa called in enfolp." << std::endl;
    out.close();
    */
#ifdef DEBUG_CONSHDLR
    std::cout << "Sepa called in enfolp." << std::endl;
#endif
   *result = SCIP_FEASIBLE;

   for( int i = 0; i < nconss; ++i )
   {
      SCIP_CONSDATA* consdata;
      //vector< vector< SCIP_VAR* > > x;
      SCIP_Bool found;
      consdata = SCIPconsGetData(conss[i]);
      assert(consdata != NULL);
      //x = consdata->x;
      //assert(x.size() != 0);

      found = findSubtour(scip, consdata, NULL);
      
      // if a subtour was found, we generate a cut constraint saying that there must be at least two outgoing edges
      if( found )
         *result = SCIP_INFEASIBLE;
   }
   
   return SCIP_OKAY;
}/*lint !e715*/

/** constraint enforcing method of constraint handler for pseudo solutions
 *
 *  The method is called at the end of the node processing loop for a node where the LP was not solved.
 *  The pseudo solution has to be checked for feasibility. If possible, an infeasibility should be resolved by
 *  branching, reducing a variable's domain to exclude the solution or adding an additional constraint.
 *  Separation is not possible, since the LP is not processed at the current node. All LP informations like
 *  LP solution, slack values, or reduced costs are invalid and must not be accessed.
 *
 *  Like in the enforcing method for LP solutions, the enforcing methods of the active constraint handlers are
 *  called in decreasing order of their enforcing priorities until the first constraint handler returned with
 *  the value SCIP_CUTOFF, SCIP_REDUCEDDOM, SCIP_CONSADDED, SCIP_BRANCHED, or SCIP_SOLVELP.
 *
 *  The first nusefulconss constraints are the ones, that are identified to likely be violated. The enforcing
 *  method should process the useful constraints first. The other nconss - nusefulconss constraints should only
 *  be enforced, if no violation was found in the useful constraints.
 *
 *  If the pseudo solution's objective value is lower than the lower bound of the node, it cannot be feasible
 *  and the enforcing method may skip it's check and set *result to SCIP_DIDNOTRUN. However, it can also process
 *  its constraints and return any other possible result code.
 *
 *  possible return values for *result (if more than one applies, the first in the list should be used):
 *  - SCIP_CUTOFF     : the node is infeasible in the variable's bounds and can be cut off
 *  - SCIP_CONSADDED  : an additional constraint was generated
 *  - SCIP_REDUCEDDOM : a variable's domain was reduced
 *  - SCIP_BRANCHED   : no changes were made to the problem, but a branching was applied to resolve an infeasibility
 *  - SCIP_SOLVELP    : at least one constraint is infeasible, and this can only be resolved by solving the SCIP_LP
 *  - SCIP_INFEASIBLE : at least one constraint is infeasible, but it was not resolved
 *  - SCIP_FEASIBLE   : all constraints of the handler are feasible
 *  - SCIP_DIDNOTRUN  : the enforcement was skipped (only possible, if objinfeasible is true)
 */
SCIP_DECL_CONSENFOPS(ConshdlrSubtour::scip_enfops)
{
    /*
    ofstream out(output_filename, std::ios::app);
    out << "Sepa called in enfops." << std::endl;
    out.close();
    */
#ifdef DEBUG_CONSHDLR
    std::cout << "Sepa called in enfops." << std::endl;
#endif
   *result = SCIP_FEASIBLE;
 
   for( int i = 0; i < nconss; ++i )
   {
      SCIP_CONSDATA* consdata;
      SCIP_Bool found;

      consdata = SCIPconsGetData(conss[i]);
      assert(consdata != NULL);
      //x = consdata->x;
      //graph = consdata->graph;
      //assert(graph != NULL);
  
      // if a subtour is found, the solution must be infeasible
      found = findSubtour(scip, consdata, NULL);      
      if( found )
         *result = SCIP_INFEASIBLE;
   }

   return SCIP_OKAY;
} /*lint !e715*/

/** feasibility check method of constraint handler for primal solutions
 *
 *  The given solution has to be checked for feasibility.
 *  
 *  The check methods of the active constraint handlers are called in decreasing order of their check
 *  priorities until the first constraint handler returned with the result SCIP_INFEASIBLE.
 *  The integrality constraint handler has a check priority of zero. A constraint handler which can
 *  (or wants) to check its constraints only for integral solutions should have a negative check priority
 *  (e.g. the alldiff-constraint can only operate on integral solutions).
 *  A constraint handler which wants to check feasibility even on non-integral solutions must have a
 *  check priority greater than zero (e.g. if the check is much faster than testing all variables for
 *  integrality).
 *
 *  In some cases, integrality conditions or rows of the current LP don't have to be checked, because their
 *  feasibility is already checked or implicitly given. In these cases, 'checkintegrality' or
 *  'checklprows' is FALSE.
 *
 *  possible return values for *result:
 *  - SCIP_INFEASIBLE : at least one constraint of the handler is infeasible
 *  - SCIP_FEASIBLE   : all constraints of the handler are feasible
 */
SCIP_DECL_CONSCHECK(ConshdlrSubtour::scip_check)
{
    /*
    ofstream out(output_filename, std::ios::app);
    out << "Sepa called in conscheck." << std::endl;
    out.close();
    */
#ifdef DEBUG_CONSHDLR
    std::cout << "Sepa called in conscheck." << std::endl;
#endif
   *result = SCIP_FEASIBLE;

   for( int i = 0; i < nconss; ++i )
   {
      SCIP_CONSDATA* consdata;
      SCIP_Bool found;

      consdata = SCIPconsGetData(conss[i]);
      assert(consdata != NULL);
      //x = consdata->x; 
      //graph = consdata->graph;
      //assert(graph != NULL);
     
      // if a subtour is found, the solution must be infeasible
      found = findSubtour(scip, consdata, sol);      
      if( found )
      {
         *result = SCIP_INFEASIBLE;
         if( printreason )
         {
            SCIP_CALL( SCIPprintCons(scip, conss[i], NULL) );
            SCIPinfoMessage(scip, NULL, "violation: graph has a subtour\n");
         }
      }
   }   


   return SCIP_OKAY;
} /*lint !e715*/


/** variable rounding lock method of constraint handler
 *
 *  This method is called, after a constraint is added or removed from the transformed problem.
 *  It should update the rounding locks of all associated variables with calls to SCIPaddVarLocksType(),
 *  depending on the way, the variable is involved in the constraint:
 *  - If the constraint may get violated by decreasing the value of a variable, it should call
 *    SCIPaddVarLocksType(scip, var, SCIP_LOCKTYPE_MODEL, nlockspos, nlocksneg), saying that rounding down is
 *    potentially rendering the (positive) constraint infeasible and rounding up is potentially rendering the
 *    negation of the constraint infeasible.
 *  - If the constraint may get violated by increasing the value of a variable, it should call
 *    SCIPaddVarLocksType(scip, var, SCIP_LOCKTYPE_MODEL, nlocksneg, nlockspos), saying that rounding up is
 *    potentially rendering the constraint's negation infeasible and rounding up is potentially rendering the
 *    constraint itself infeasible.
 *  - If the constraint may get violated by changing the variable in any direction, it should call
 *    SCIPaddVarLocksType(scip, var, SCIP_LOCKTYPE_MODEL, nlockspos + nlocksneg, nlockspos + nlocksneg).
 *
 *  Consider the linear constraint "3x -5y +2z <= 7" as an example. The variable rounding lock method of the
 *  linear constraint handler should call SCIPaddVarLocksType(scip, x, SCIP_LOCKTYPE_MODEL, nlocksneg, nlockspos),
 *  SCIPaddVarLocksType(scip, y, SCIP_LOCKTYPE_MODEL, nlockspos, nlocksneg) and
 *  SCIPaddVarLocksType(scip, z, SCIP_LOCKTYPE_MODEL, nlocksneg, nlockspos) to tell SCIP,
 *  that rounding up of x and z and rounding down of y can destroy the feasibility of the constraint, while rounding
 *  down of x and z and rounding up of y can destroy the feasibility of the constraint's negation "3x -5y +2z > 7".
 *  A linear constraint "2 <= 3x -5y +2z <= 7" should call
 *  SCIPaddVarLocksType(scip, ..., SCIP_LOCKTYPE_MODEL, nlockspos + nlocksneg, nlockspos + nlocksneg) on all variables,
 *  since rounding in both directions of each variable can destroy both the feasibility of the constraint and it's negation
 *  "3x -5y +2z < 2  or  3x -5y +2z > 7".
 *
 *  If the constraint itself contains other constraints as sub constraints (e.g. the "or" constraint concatenation
 *  "c(x) or d(x)"), the rounding lock methods of these constraints should be called in a proper way.
 *  - If the constraint may get violated by the violation of the sub constraint c, it should call
 *    SCIPaddConsLocks(scip, c, nlockspos, nlocksneg), saying that infeasibility of c may lead to infeasibility of
 *    the (positive) constraint, and infeasibility of c's negation (i.e. feasibility of c) may lead to infeasibility
 *    of the constraint's negation (i.e. feasibility of the constraint).
 *  - If the constraint may get violated by the feasibility of the sub constraint c, it should call
 *    SCIPaddConsLocks(scip, c, nlocksneg, nlockspos), saying that infeasibility of c may lead to infeasibility of
 *    the constraint's negation (i.e. feasibility of the constraint), and infeasibility of c's negation (i.e. feasibility
 *    of c) may lead to infeasibility of the (positive) constraint.
 *  - If the constraint may get violated by any change in the feasibility of the sub constraint c, it should call
 *    SCIPaddConsLocks(scip, c, nlockspos + nlocksneg, nlockspos + nlocksneg).
 *
 *  Consider the or concatenation "c(x) or d(x)". The variable rounding lock method of the or constraint handler
 *  should call SCIPaddConsLocks(scip, c, nlockspos, nlocksneg) and SCIPaddConsLocks(scip, d, nlockspos, nlocksneg)
 *  to tell SCIP, that infeasibility of c and d can lead to infeasibility of "c(x) or d(x)".
 *
 *  As a second example, consider the equivalence constraint "y <-> c(x)" with variable y and constraint c. The
 *  constraint demands, that y == 1 if and only if c(x) is satisfied. The variable lock method of the corresponding
 *  constraint handler should call SCIPaddVarLocksType(scip, y, SCIP_LOCKTYPE_MODEL, nlockspos + nlocksneg, nlockspos + nlocksneg) and
 *  SCIPaddConsLocks(scip, c, nlockspos + nlocksneg, nlockspos + nlocksneg), because any modification to the
 *  value of y or to the feasibility of c can alter the feasibility of the equivalence constraint.
 */
SCIP_DECL_CONSLOCK(ConshdlrSubtour::scip_lock)
{
   /* 
    ofstream out(output_filename, std::ios::app);
    out << "Sepa called in conslock." << std::endl;
    out.close();
    */
#ifdef DEBUG_CONSHDLR
    //std::cout << "Sepa called in conslock." << std::endl;
#endif
   SCIP_CONSDATA* consdata;
   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);
    auto& x = *(consdata->x);
    auto& z = *(consdata->z);
   assert( z.size() == x.size() );
   int n = x.size();

   for (int i=0; i < n; i++)
   {
      assert( z[i] != NULL );
       // both rounding up or down visit vars may lead to infeasibility
      SCIP_CALL( SCIPaddVarLocksType(
                  scip, z[i], SCIP_LOCKTYPE_MODEL, nlockspos+nlocksneg, nlockspos+nlocksneg) );
      for (int j=0; j < i; j++) {
          assert( x[i][j] != NULL );
           // rouding up edge vars create subtours, rounding down is safe
          SCIP_CALL( SCIPaddVarLocksType(
                      scip, x[i][j], SCIP_LOCKTYPE_MODEL, nlockspos+nlocksneg, nlockspos+nlocksneg) );
      }
   }
   return SCIP_OKAY;
} /*lint !e715*/



// ----------------------
// Additional callbacks 
// ----------------------

/** separation method of constraint handler for LP solution
 *
 *  Separates all constraints of the constraint handler. The method is called in the LP solution loop,
 *  which means that a valid LP solution exists.
 *
 *  The first nusefulconss constraints are the ones, that are identified to likely be violated. The separation
 *  method should process only the useful constraints in most runs, and only occasionally the remaining
 *  nconss - nusefulconss constraints.
 *
 *  possible return values for *result (if more than one applies, the first in the list should be used):
 *  - SCIP_CUTOFF     : the node is infeasible in the variable's bounds and can be cut off
 *  - SCIP_CONSADDED  : an additional constraint was generated
 *  - SCIP_REDUCEDDOM : a variable's domain was reduced
 *  - SCIP_SEPARATED  : a cutting plane was generated
 *  - SCIP_DIDNOTFIND : the separator searched, but did not find domain reductions, cutting planes, or cut constraints
 *  - SCIP_DIDNOTRUN  : the separator was skipped
 *  - SCIP_DELAYED    : the separator was skipped, but should be called again
 */
SCIP_DECL_CONSSEPALP(ConshdlrSubtour::scip_sepalp)
{
    /*
    ofstream out(output_filename, std::ios::app);
    out << "Sepa called in conssepalp." << std::endl;
    out.close();
    */
#ifdef DEBUG_SUBTOUR
    std::cout << "Sepa called in conssepalp." << std::endl;
#endif
   SCIP_CALL( sepaSubtour(scip, conshdlr, conss, nconss, nusefulconss, NULL, result) );

   return SCIP_OKAY;
}


/** separation method of constraint handler for arbitrary primal solution
 *
 *  Separates all constraints of the constraint handler. The method is called outside the LP solution loop (e.g., by
 *  a relaxator or a primal heuristic), which means that there is no valid LP solution.
 *  Instead, the method should produce cuts that separate the given solution.
 *
 *  The first nusefulconss constraints are the ones, that are identified to likely be violated. The separation
 *  method should process only the useful constraints in most runs, and only occasionally the remaining
 *  nconss - nusefulconss constraints.
 *
 *  possible return values for *result (if more than one applies, the first in the list should be used):
 *  - SCIP_CUTOFF     : the node is infeasible in the variable's bounds and can be cut off
 *  - SCIP_CONSADDED  : an additional constraint was generated
 *  - SCIP_REDUCEDDOM : a variable's domain was reduced
 *  - SCIP_SEPARATED  : a cutting plane was generated
 *  - SCIP_DIDNOTFIND : the separator searched, but did not find domain reductions, cutting planes, or cut constraints
 *  - SCIP_DIDNOTRUN  : the separator was skipped
 *  - SCIP_DELAYED    : the separator was skipped, but should be called again
 */
SCIP_DECL_CONSSEPASOL(ConshdlrSubtour::scip_sepasol)
{
    /*
    ofstream out(output_filename, std::ios::app);
    out << "Sepa called in conssepasol." << std::endl;
    out.close();
    */
#ifdef DEBUG_CONSHDLR
    std::cout << "Sepa called in conssepsol." << std::endl;
#endif
   SCIP_CALL( sepaSubtour(scip, conshdlr, conss, nconss, nusefulconss, sol, result) );

   return SCIP_OKAY;
}


/** frees specific constraint data
 *
 *  WARNING! There may exist unprocessed events. For example, a variable's bound may have been already changed, but
 *  the corresponding bound change event was not yet processed.
 */

SCIP_DECL_CONSDELETE(ConshdlrSubtour::scip_delete)
{
    return SCIP_OKAY;
    std::cout << "called in consdelete" << std::endl;
   assert(consdata != NULL);
   auto x = (*consdata)->x;
   auto z = (*consdata)->z;
   assert( x->size() == z->size() );
   for (int i=0; i < x->size(); i++)
   {
       SCIPreleaseVar(scip, &((*z)[i]));
       for (int j=0; j<i; j++)
       {
           SCIPreleaseVar(scip, &((*x)[i][j]));
       }
   }
   SCIPfreeBlockMemory(scip, consdata);

   return SCIP_OKAY;

}/*lint !e715*/


/** transforms constraint data into data belonging to the transformed problem */
SCIP_DECL_CONSTRANS(ConshdlrSubtour::scip_trans)
{
#ifdef DEBUG_CONSHDLR
    std::cout << "Sepa called in constrans." << std::endl;
#endif
   SCIP_CONSDATA* sourcedata;
   SCIP_CONSDATA* targetdata;

   sourcedata = SCIPconsGetData(sourcecons);
   assert( sourcedata != NULL );
   targetdata = NULL;

   SCIP_CALL( SCIPallocBlockMemory(scip, &targetdata) );
   targetdata->x = sourcedata->x;
   targetdata->z = sourcedata->z;
   /* create target constraint */
   SCIP_CALL( SCIPcreateCons
                   (scip, 
                    targetcons, 
                    SCIPconsGetName(sourcecons), 
                    conshdlr, 
                    targetdata,
                    SCIPconsIsInitial(sourcecons), 
                    SCIPconsIsSeparated(sourcecons), 
                    SCIPconsIsEnforced(sourcecons),
                    SCIPconsIsChecked(sourcecons), 
                    SCIPconsIsPropagated(sourcecons),  
                    SCIPconsIsLocal(sourcecons),
                    SCIPconsIsModifiable(sourcecons), 
                    SCIPconsIsDynamic(sourcecons), 
                    SCIPconsIsRemovable(sourcecons),
                    SCIPconsIsStickingAtNode(sourcecons)
                    ) 
                   );

   return SCIP_OKAY;
}


/** domain propagation method of constraint handler
 *
 *  The first nusefulconss constraints are the ones, that are identified to likely be violated. The propagation
 *  method should process only the useful constraints in most runs, and only occasionally the remaining
 *  nconss - nusefulconss constraints.
 *
 *  possible return values for *result:
 *  - SCIP_CUTOFF     : the node is infeasible in the variable's bounds and can be cut off
 *  - SCIP_REDUCEDDOM : at least one domain reduction was found
 *  - SCIP_DIDNOTFIND : the propagator searched, but did not find any domain reductions
 *  - SCIP_DIDNOTRUN  : the propagator was skipped
 *  - SCIP_DELAYED    : the propagator was skipped, but should be called again
 */
SCIP_DECL_CONSPROP(ConshdlrSubtour::scip_prop)
{
   assert(result != NULL);
   *result = SCIP_DIDNOTRUN;
   return SCIP_OKAY;
} /*lint !e715*/


/** variable deletion method of constraint handler
 *
 *  This method should iterate over all constraints of the constraint handler and delete all variables
 *  that were marked for deletion by SCIPdelVar().
 *
 *  input:
 *  - scip            : SCIP main data structure
 *  - conshdlr        : the constraint handler itself
 *  - conss           : array of constraints in transformed problem
 *  - nconss          : number of constraints in transformed problem
 */
SCIP_DECL_CONSDELVARS(ConshdlrSubtour::scip_delvars)
{
   return SCIP_OKAY;
} /*lint !e715*/


/** constraint display method of constraint handler
 *
 *  The constraint handler should store a representation of the constraint into the given text file.
 */
/*
SCIP_DECL_CONSPRINT(ConshdlrSubtour::scip_print)
{
   SCIP_CONSDATA* consdata;
   GRAPH* g;
   
   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);
      
   g = consdata->graph;
   assert(g != NULL);

   SCIPinfoMessage(scip, file, "subtour of Graph G with %d nodes and %d edges\n", g->nnodes, g->nedges);
   
   return SCIP_OKAY;
}*/ /*lint !e715*/

/** clone method which will be used to copy a objective plugin */
SCIP_DECL_CONSHDLRCLONE(ObjProbCloneable* ConshdlrSubtour::clone) /*lint !e665*/
{
   *valid = true;
   return new ConshdlrSubtour(scip);
}

/** constraint copying method of constraint handler
 *
 *  The constraint handler can provide a copy method, which copies a constraint from one SCIP data structure into a other
 *  SCIP data structure.
 */
SCIP_DECL_CONSCOPY(ConshdlrSubtour::scip_copy)
{
#ifdef DEBUG_CONSHDLR
    std::cout << "Sepa called in conscopy." << std::endl;
#endif
   SCIP_CONSHDLR* conshdlr;
   SCIP_CONSDATA* consdata;
   SCIP_CONSDATA* sourcedata;
   
   /* find the subtour constraint handler */
   conshdlr = SCIPfindConshdlr(scip, "subtour");
   if( conshdlr == NULL )
   {
      SCIPerrorMessage("subtour constraint handler not found\n");
      return SCIP_PLUGINNOTFOUND;
   }

   sourcedata = SCIPconsGetData(sourcecons);
   assert( sourcedata != NULL );
   consdata = NULL;
   SCIP_CALL( SCIPallocBlockMemory( scip, &consdata) );
   consdata->x = sourcedata->x;
   consdata->z = sourcedata->z;

   /* create constraint */
   SCIP_CALL( SCIPcreateCons(scip, cons, (name == NULL) ? SCIPconsGetName(sourcecons) : name, 
         conshdlr, consdata, initial, separate, enforce, check, 
         propagate, local, modifiable, dynamic, removable, FALSE) );

   *valid = true;
   return SCIP_OKAY;
} /*lint !e715*/

/** creates and captures a TSP subtour constraint */
SCIP_RETCODE tsp::SCIPcreateConsSubtour(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   vector< vector< SCIP_VAR* > >& x,          /**< SCIP x variables */
   vector< SCIP_VAR* >&  z,                  /**< visit indicator var */
   int k,
   int t,
   SCIP_Bool             initial,            /**< should the LP relaxation of constraint be in the initial LP? */
   SCIP_Bool             separate,           /**< should the constraint be separated during LP processing? */
   SCIP_Bool             enforce,            /**< should the constraint be enforced during node processing? */
   SCIP_Bool             check,              /**< should the constraint be checked for feasibility? */
   SCIP_Bool             propagate,          /**< should the constraint be propagated during node processing? */
   SCIP_Bool             local,              /**< is constraint only valid locally? */
   SCIP_Bool             modifiable,         /**< is constraint modifiable (subject to column generation)? */
   SCIP_Bool             dynamic,            /**< is constraint dynamic? */
   SCIP_Bool             removable           /**< should the constraint be removed from the LP due to aging or cleanup? */
   )
{
   assert( x.size() == z.size() );
   int n = x.size();
   SCIP_CONSHDLR* conshdlr;
   SCIP_CONSDATA* consdata;

   /* find the subtour constraint handler */
   conshdlr = SCIPfindConshdlr(scip, "subtour");
   if( conshdlr == NULL )
   {
      SCIPerrorMessage("subtour constraint handler not found\n");
      return SCIP_PLUGINNOTFOUND;
   }

   /* create constraint data */
   // seems bug is the following memory allocation line
   // consdata->x.size() is undefined
   consdata = NULL;
   SCIP_CALL( SCIPallocBlockMemory( scip, &consdata) ); /*lint !e530*/
   assert( consdata != NULL );
    // below stuck
   consdata->x = &x;
   consdata->z = &z;
#ifdef DEBUG_CONSHDLR
   std::cout << "name: " << name << std::endl;
#endif
   /* create constraint */
   SCIP_CALL( SCIPcreateCons(scip, cons, name, conshdlr, consdata, initial, separate, enforce, check, propagate,
         local, modifiable, dynamic, removable, FALSE) );
#ifdef DEBUG_CONSHDLR
   std::cout << "Finished creating the constraint." << std::endl;
#endif

   return SCIP_OKAY;
}
