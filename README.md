# Summary
Code for solving Capacitated Inventory Routing Problem (CIRP) via local search

# Dependencies
[dapcstp](https://github.com/mluipersbeck/dapcstp)

[Concorde](http://www.math.uwaterloo.ca/tsp/concorde/downloads/downloads.htm)

[SCIP](https://www.scipopt.org)

# Installation
* Step 1:
    Build above dependencies according to the installation guide provided by each software. For the lower bound computation, as explained in the online supplement, we used SCIP-6.0.2 built with Cplex 12.10 for the branch-and-cut algorithm.

* Step 2:
    Go to the directory of the irp-code repo. Modify line 2 in CMakeLists.txt so that SCIP_BUILD points to the correct location of the build directory of SCIP. Create a folder called 'build'. Place the compiled binaries 'dapcstp' and 'concorde' under this build directory.

* Step 3:

``` console
cd build
cmake ..
make
```

# Usage
To reproduce test results, run two binaries:
``` console
./main_heur
./main_mip
```
Results are stored in heur.txt and mip.txt respectively. To solve other instances, modify the main functions accordingly.
