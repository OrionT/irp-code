#ifndef INST_H_
#define INST_H_

#include<string>
#include<vector>
#include<unordered_set>

using namespace std;
typedef vector<double> Double1d;
typedef vector<Double1d> Double2d;
typedef vector<Double2d> Double3d;

// Debug macro
//#define DEBUG

#ifdef DEBUG
#define DEBUG_MSG(str) do {cerr << str << std::endl; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif
struct Inst {
    int N;
    int T;
    Double2d demand;
    Double3d H;
    Double2d w;
    double truck_capacity;
    int num_trucks;
    double large_penalty;
    Double2d position;
};

// implement visit_set as vector of unordered sets
// since we do not care about which edges are in the graph
// nor do we care about the order of the nodes visited by PCST
typedef unordered_set<int> Visit;
typedef vector<Visit> Visit_set; // set of visits

// some comments about latest_visit and t_hat
// in fact, latest_visit is used to find the index for H (holding cost, 3d double) to compare the saving with and t_hat is used for determining upper bounds on the summation symbol when calculating saving 
struct Solution {
    Visit_set visit_set;
    double total_holding;
    vector<double> routing_per_day;
    vector<vector<int> > latest_visit;
    vector<vector<int> > t_hat;
    vector<double> slack_capacity;
};

// TourSet holds a set of tours on a particular day, each is an ordered ints
typedef vector<vector<int> > TourSet;

// TourSol is a the final solution of IRP, consisting of vector of TourSets
typedef vector<TourSet> TourSol;

//for declaring size n vector with entries of type T
template<class T> vector<T> init1D(int n)
{
	vector<T> result;
	result.resize(n);
	return result;
}

//for declaring size n by m vector with entries of type T
template<class T> vector<vector<T> > init2D(int n, int m)
{
	vector<vector<T> > result;
	result.resize(n);
	for (int i = 0; i < n; i++) {
		result[i].resize(m);
	}
	return result;
}

//for declaring size n by m by p vector with entries of type T
template<class T> vector<vector<vector<T> > > init3D(int n, int m, int p)
{
	vector<vector<vector<T> > > result;
	result.resize(n);
	for (int i = 0; i < n; i++) {
		result[i].resize(m);
		for (int j = 0; j < m; j++) {
			result[i][j].resize(p);
		}
	}
	return result;
}

void loadData(string filename, int& N, int& T, Double2d& demand, Double3d& holding_cost, Double2d& w, double& truck_capacity, int& num_trucks, Double2d& position);

// This function computes holding cost H[v][s][t] defined as the cost incurred on day t when d^v_t is delivered on day s (cumulative fashion)
void initHoldingCost(Double3d& holding_cost, const Double1d& unit_holding, const Double2d& demand, const int N, const int T);

// This function computes pairwise Euclidean distance
void getDistance(Double2d& w, const Double2d& position, const int& N);

// This function checks if the holding corresponds to the current routing plan
// and outputs info about solution in DEBUG mode
void checkSolCorrectness(const Inst& inst, const Solution& sol);

double getTotalHolding(const Inst& inst, const Solution& sol);

// This function computes total cost from sol
double getTotalCost(const Solution& sol);

// This function computes the amount of demand to delivery on each day, given a solution
void getDemandToDelivery(const Inst& inst, const Solution& sol, Double2d& demand_to_deliver);

// This function writes to a txt file in "../result/filename" where line i 
// indicates the delivery plan for day i. Line i is an array of demands for node 0, ..., inst.N-1
void writeSol(const Inst& inst, const Solution& sol, const vector<double>& tour_cost_per_day, string filename);

// a repetitive function used to write sol for zero holding inital solution (running out of time for journal submission, could possibly combine with the above)
void writeSolZeroHolding(const Inst& inst, const Solution& sol, const vector<double>& tour_cost_per_day, string filename);

vector<double> concordeTourHeuristic(const Inst& inst, const Solution& sol);

double getTourFromConcorde(vector<int>& tour, const Inst& inst, const Solution& sol, const int day);

double getCostForBreakupTourAndReoptimize(const vector<int>& tour, const Inst& inst, const Solution& sol, const int day);

double getCostForBreakingUpTour(const vector<int>& tour, const Inst& inst, const Solution& sol, const int day); 
/*
// This function returns the final tour solution (vector of vector of tours)
TourSol getTourFromORTool(const Solution& sol, const Inst& inst);

// This function returns the set of tours on a particular day
TourSet getTourOnDay(const Solution& sol, const Inst& inst, const int day);
*/
void writeDataForConcorde(const vector<vector<double> >& position, const Visit& visit, vector<int>& map_from_concorde_label_to_true_label);

double readSolFromConcorde(vector<int>& tour, const Inst& inst, const Visit& visit, const vector<int>& mapping);

vector<int> getCycleStartingFromDepot(const vector<int>& cycle_old, const int depot_index=0);

void writeDataForConcorde(const vector<vector<int> >& position, const Visit& visit);
#endif
