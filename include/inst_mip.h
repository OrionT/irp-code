#ifndef INST_H_
#define INST_H_

#include <vector>
#include <numeric>

using namespace std;
typedef vector<double> Double1d;
typedef vector<Double1d> Double2d;
typedef vector<Double2d> Double3d;

#ifdef DEBUG
#define DEBUG_MSG(str) do {cerr << str << std::endl; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif
void loadData(const char* filename, Double2d& d, Double1d& h, Double2d& c, double& Q);

// This function computes pairwise Euclidean distance
void getDistance(Double2d& w, const Double2d& position, const int& N);

template <typename T> vector<size_t> sort_indices(const vector<T> &v, bool descending) 
{
  // initialize original index locations
  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);
  // sort indexes based on comparing values in v
  sort(idx.begin(), idx.end(),
       [&v, descending](size_t i1, size_t i2)->bool {
       if (descending) return v[i1] > v[i2];
       else return v[i1] < v[i2];});

  return idx;
}

#endif
