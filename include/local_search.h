#ifndef LOCAL_SEARCH_H_
#define LOCAL_SEARCH_H_
#include <map>
#include "inst.h"
#include<unordered_set>

using namespace std;

struct LocalSearchResult {
    int day;
    unordered_set<int> pcst_visit;
    double routing;
    double holding_difference;
};


enum LocalSearchOperation {
    kEdit = 0,
    kAddWithNoProxy,
    kDelWithNoProxy,
};

// route demands via MST periodically
void dumbUpdateSlack(const Inst& inst, Solution& sol);

void initPeriodicMST(Solution& sol, const Inst& inst);

// inits a solution which visits everyone everyday, based on sol which is periodic MST (I'm being lazy to call PCST solver, so this function might not be robust)
Solution initSolWithZeroHolding(const Solution& sol, const Inst& inst);

// initialize solution from the periodic MST computed inside initPeriodicMST function
void initSol(Solution& sol, const vector<int>& delivery_day, const double routing_cost, const Inst& inst);

// This function calls dapcst solver for steiner tree instance given by the input
void getPCST(unordered_set<int>& visit, double& routing_cost, const Inst& inst, const vector<double>& _penalties);

// This function computes local search on sol (initial feasible solution)
void localSearch(const Inst& inst, Solution& sol, const double ter, const int max_iter, double& time);

// This function computes one step of local search based on operation type and returns the one with the most saving in PCST cost
LocalSearchResult localSearchPerOperation(const LocalSearchOperation operation, const Inst& inst, const Solution& sol);

// This function computes the result after applying one step of local search on day [day]
// we implement heuristic lagrangian multiplier to maintain feasibility in this function
LocalSearchResult searchOnDay(const LocalSearchOperation operation, const Inst& inst, const Solution& sol, const int day);

// This function computes penalty of all nodes based on a specific operation
vector<double> getPenalty(LocalSearchOperation operation, const Inst& inst, const Solution& sol, const int day);

// This function computes the saving that node v can get if it is visited on [day], given the current sollution sol
double getSaving(const Inst& inst, const Solution& sol, const int day, const int v);

// This function computes the CVPR proxy (the amount that should be deducted from saving), given the current solution and demand
double getCVPR_Proxy(const Inst& inst, const Solution& sol, const int day, const int v);

// This function computes the holding cost difference btw changing [day] to new_visit
double getHoldingDifference(const Inst& inst, const Solution& sol, const int day, const unordered_set<int>& new_visit);

// This function computes s1-s2
template<typename T> unordered_set<T> getSetDifference(const unordered_set<T>& s1, const unordered_set<T>& s2);

// This function updates the solution with result
void updateSol(Solution& sol, const LocalSearchResult& result, const Inst& inst);

// This function picks the best local search result from all possible operations, w.r.t. change in solution value
LocalSearchResult pickBestResult(const Solution& sol, const vector<LocalSearchResult>& vec_result, double& improve_ratio, size_t& which_best);

// This function computes demand to deliver for each node on a particular day
void getDemandToDeliver(vector<double>& demand_per_node, const Inst& inst, const Solution& sol, const int day);

// This function computes the partition of nodes who are not visited today by their latest_visit
void getNodePartition(map<int, unordered_set<int> >& node_partition, const Solution& sol, const Inst& inst, const int day, const vector<double>& demand_per_node);

// This function asserts whether the given slack vector is entrywise nonnegative
bool isFeasible(const vector<double>& slack); 

// generate a slack vector after replacing the old visit on day *day$ of solution by visit
vector<double> getSlackFromNewVisit(const Visit& visit, const Inst& inst, const Solution& sol, const int day);

// computes feasible upper bound for multiplier of add operation
double getUbForAdd(const Inst& inst, const Solution& sol, const vector<double>& penalty, const int day);

//vector<double> getNewPenaltyForAdd(const Inst& inst, const Visit& visit, const vector<double>& penalty, const vector<double>& demand, const double guess);

// This function checks if upper bound ub is feasible (i.e. slacks are all non-negative 
//bool isFeasibleUbForAdd(const Inst& inst, const Solution& sol, const vector<double>& penalty, const vector<double>& demand_per_node, const int day, const double ub);

// computes feasible upper bound for delete operation
//double getUbForDel(const Inst& inst, const Solution& sol, const vector<double>& demand_per_node, const map<int, unordered_set<int> >& node_partition, const vector<double>& test_penalty, const int day, double guess=0.1);

//bool isFeasibleUbForDel(const Inst& inst, const Solution& sol, const vector<double>& penalty, const vector<double>& demand_per_node, const int day, const double ub);

/*
vector<double> binarySearchForPenalty(const Inst& inst, const Solution& sol, const vector<double>& demand_unvisited_nodes, double ub);

vector<double> binarySearchForPenalty(const Inst& inst, const Solution& sol, const map<int, unordered_set<int> >& node_partition, double ub);
*/
LocalSearchResult getResultFromVisit(const Visit& visit, const double routing_cost, const int day, const Inst& inst, const Solution& sol);

void LagForAdd(Visit& visit, double& routing_cost, const Inst& inst, const Solution& sol, const int day, const vector<double>& penalty);

double getLagValForAdd(const Visit& visit, const Inst& inst, const vector<double>& penalty, const double routing_cost, const double multiplier, const double grad);

double getGradForAdd(const vector<double>& demand, const Visit& visit, const Solution& sol, const int day);

void updateVirtualPenaltyForAdd(const Visit& visit, vector<double>& penalty, const double multiplier, const vector<double>& demand);

void LagForDel(Visit& visit, double& routing_cost, const Inst& inst, const Solution& sol, const int day, const vector<double>& penalty);

map<int, double> getGradForDel(const vector<double>& demand, const Visit& visit, const Solution& sol, const map<int, unordered_set<int> >& node_partition);

double getLagValForDel(const Visit& visit, const Solution& sol, const int day, const vector<double>& penalty, const double routing_cost, const map<int, double>& multiplier, const map<int, double>& grad);

double getStepsizeForDel(const double ub, const double lag_val, const double eps, const map<int, double>& grad);

void updateVirtualPenaltyForDel(vector<double>& virtual_penalty, const map<int, double>& multiplier, const vector<double>& demand, const Solution& sol, const int day);

void getVisitAndRoutingCostFromLagRelax(Visit& visit, double& routing_cost, const Inst& inst, const Solution& sol, const int day, const vector<double> penalty, const LocalSearchOperation operation);
#endif
