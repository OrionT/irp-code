cmake_minimum_required(VERSION 3.3)
set(SCIP_BUILD /Users/orion/Desktop/project/scipoptsuite-6.0.2/build)
list(APPEND CMAKE_PREFIX_PATH ${SCIP_BUILD})

project(cirp)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_compile_options(-DIL_STD -O -fPIC -fexceptions -m64)
find_package(SCIP REQUIRED)

include_directories(
    ${CMAKE_SOURCE_DIR}/include
    ${SCIP_INCLUDE_DIRS}
    /usr/local/Cellar/boost/1.71.0/include
    )

add_executable(
    main_heur
    src/main_local_search.cc
    src/local_search.cc
    src/inst.cc
    )

add_executable(
    main_mip
    src/main_mip.cpp
    src/ConshdlrGSEC.cpp
    src/inst_mip.cpp
    )

target_link_libraries(main_mip ${SCIP_LIBRARIES})
